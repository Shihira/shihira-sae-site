def md2html(text, encoding="utf-8"):
        import markdown
        text = text.decode("utf-8")
        html = markdown.markdown(text)
        html = html.encode("utf-8")
        return html

