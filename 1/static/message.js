// Copyright 2013 Shihira Fung.
// E-mail: fengzhiping@hotmail.com
// Weibo: @Shihira
var browser_type = 0; // 0 : Mobile; 1 : PC/iPad

$(document).ready(function() {

function gen_url() {
        $("#loading_pic").css("display", "inline");
        var url = ajax_req;
        var from = $("div.content_0").size() + $("div.content_1").size();
        url += "&from=" + from;
        
        return url;
}

function adjust_left_side() {
        if(browser_type) $(".left-side").css("width", "800px");
        $(".left-side").each(function(index) {
                $(this).html(gen_msg($(this)));
        });
}

function show_new(data) {
        $("#history").prepend(data);
        $("#loading_pic").css("display", "none");
        adjust_left_side();
}



function get_new() { $.get(gen_url(), show_new); }
function send_msg() { $.post(gen_url(), $("#msg_text").val(), show_new); }
function gen_msg(element) {
        var img = element.attr("piu");
        var weibo_addr = element.attr("addr");
        var weibo_id = element.attr("sn");
        var content = element.attr("cont");

        var table = '<table border="0" cellspacing="0" cellpadding="0" width="100%"> <tr>' +
        '    <td rowspan="2" valign="top" id="image_column"><img src="' + img + '" /></td>' +
        '    <td id="id_row"><a href="' + weibo_addr + '">@' + weibo_id + '</a></td></tr><tr>' +
        '    <td style="padding-left: 5px; background-color: #EEE;">' + decodeURIComponent(content) + '</td></tr>' +
		'</table>';
        return table
}



$("#loading_pic").css("display", "none");
$("#msg_btn").click(send_msg);

if($(".page-title").width() > 800) browser_type = 1;
adjust_left_side()

setInterval(get_new, 10000);


});
