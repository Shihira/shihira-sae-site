import weibo
from database import *
import webutil as wu

def get_client(callback):
        client = weibo.APIClient(app_key = "122146044",
                app_secret = "121d13be3f7b8c6705d335f45848ea2a",
                redirect_uri = callback)
        return client

def get_redirect_uri(environ):
        redirect_uri = wu.reconstruct_url(environ, path_info = False) + \
                "/authorize.py?url=" + wu.reconstruct_url(environ)
        return redirect_uri

class User(object):
        def get_users_show(self):
                self.users_show = self.client.users.show.get(uid = self.uid)

        def __init__(self, environ, uid = None, arg = "user"):
                self.client = get_client(get_redirect_uri(environ))

                sql = "select * from `access_tokens` where uid = %s;"
                if uid: sql = sql % str(uid)
                else: sql = sql % str(wu.urlargs(environ)[arg])
                result = sql_query(sql)
                self.token_info = dict(zip(("uid", "code", "access_token",
                        "expires_in", "screen_name", "create_in"), result[0]))
                self.users_show = None
                self.client.set_access_token(self.access_token, self.expires_in)

        def __getattr__(self, attr):
                if attr in self.token_info:
                        return self.token_info[attr]
                elif self.users_show:
                        return getattr(self.users_show, attr)


