from dict_base import dict_base

class exconcise_ec(dict_base):
        def entry(self, index, no_expl = False):
                father = super(exconcise_ec, self)

                import re
                ret = father.entry(index, no_expl)

                if not no_expl:
                        sp = re.split(u"(?<=[a-z])[.]|[,|\uff0c|]", ret[1])

                        expl = {"pos":[], "field":[], "def":[]}
                        for item in sp:
                                if re.match("[a-z]+", item):
                                        expl["pos"] += [item + "."]
                                elif item:
                                        expl["def"] += [item]
                        expl["pos"] = ",".join(expl["pos"])
                        ret[1] = [[expl], []]

                return ret

        def __init__(self):
                father = super(exconcise_ec, self)

                father.__init__()
                self.load_pair("exconcise_ec")

#import random
#print exconcise_ec().entry(random.randint(0, 7000))
