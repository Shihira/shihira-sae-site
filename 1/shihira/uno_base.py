#### uno_base.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import random
import webutil as wu
from database import *

def cb(var, room): return var + "@" + room
def sp(var): return tuple(var.split("@"))

class Card(object):
        color_dict = { 0: "#F00", 1: "#070",
        2: "#06C", 3: "#FC0", 4: "#666" }
        action_card = ["x", "=", "+2"]
        wildcard = ["+4", "?"]

        def __init__(self, (color, number)=(None, None)):
                if number == None:
                        if color == 4:
                                number = random.choice(wildcard)
                        else:number = random.choice(range(0, 10) +
                                self.action_card + self.wildcard)
                if color == None:
                        color = random.randint(0, 3)
                        if number in self.wildcard: color = 4

                self.color = int(color)
                self.number = str(number)

        def __str__(self):
                return '{"color":"%s","number":"%s"}' % \
                        (self.color_dict[self.color], self.number)

        def able_follow(self, card):
                if not "+" in card.number:
                        if card.color == self.color: return True
                        if card.number == self.number: return True
                        if self.color == 4: return True
                elif "+" in self.number: return True
                return False

        def able_combo(self, card):
                if card.number != self.number: return False
                else: return True

        def __cmp__(self, cardr):
                if type(cardr) != type(self): return -1
                if self.color > cardr.color:
                        return 1
                elif self.color == cardr.color:
                        if self.number > cardr.number: return 1
                        elif self.number == cardr.number: return 0
                        else: return -1
                else:   return -1

class Player(object):
        def __init__(self, name, hand = None):
                self.name = None
                self.hand = [] # hand means a card list

                self.name = name
                if hand != None: self.hand = hand; return
                sql = "select `color`, `number` from `uno_player` " \
                      "where `player`='%s' order by `color`, `number`;" % name
                for card in sql_query(sql):
                        self.hand += [Card(card)]

        def save(self):
                # sql =  "delete from `uno_player` " \
                #        "where `player`='%s';" % self.name
                # sql_query(sql)

                if len(self.hand) == 0: return
                for card in self.hand:
                        sql = "insert into `uno_player` values ('%s', %d, '%s');" \
                                % (self.name, card.color, card.number)
                        sql_query(sql)

        def __str__(self):
                json = ""
                for card in self.hand:
                        json += "," + str(card)
                return json[1:]

        def realname(self): return sp(self.name)[0]


