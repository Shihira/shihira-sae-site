import web
import ce_balancer_lib.equation as ce

class page:
        def GET(self):
                render = web.template.render("templates/", base = "framework")
                query = web.input()

                inputce = None
                state = None
                result = None

                if "ce" not in query or not query.ce:
                        inputce = ""
                        state = "example"
                        result = []
                        examples = ["H2SO4 + K4Fe(CN)6 + KMnO4 = CO2 + "
                                        "Fe2(SO4)3 + H2O + HNO3 + KHSO4 + MnSO4",
                                  "FeE2 + Cl2 = FeE3 + ClE-",
                                  "Al+NaOH+H2O=NaAlO2+H2",
                                  "Cu_H2SO4:CuSO4_SO2_H2O"]

                        import urllib
                        for example in examples:
                                result += [(example, urllib.quote(example))]
                else:
                        eq = ""
                        inputce = query.ce.encode("utf8")
                        try:
                                eq = ce.equation(inputce)
                                eq.balance()

                                state = "succeeded"
                                result = str(eq)
                        except:
                                state = "failed"
                                result = [str(eq), eq.get_matrix(True)]

                print state, result
                return render.ce_balancer(inputce, state, result)
