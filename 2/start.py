#### start.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import shihira.webutil as wu

def app(environ, start_response):
        status = "200 OK" 
        headers = [("Content-type", "text/html")] 
        start_response(status, headers)
        
        index_html = wu.html_header

        index_html += wu.html_sub_title % wu.html_link % ("match_list.py?date=2013-04-18", "Match List")
        wu.html_content_index = 0
        index_html += wu.html_content("match_list.py gets the current NBA score as an XML, used by " +
                        wu.html_link % ("http://www.oschina.net/p/shihira-scoreboard", "Shihira-Scoreboard"))

        index_html += wu.html_sub_title % wu.html_link % ("balance_ce.py", "Balance Chemical Equation")
        wu.html_content_index = 0
        index_html += wu.html_content("balance_ce.py balances an unbalanced chemical equation; the start page has examples.")

        index_html += wu.html_sub_title % wu.html_link % ("message.py?command=msg&ui=1", "Message")
        wu.html_content_index = 0
        index_html += wu.html_content("It's a simple chat room, with no user limitation.")

        index_html += wu.html_sub_title % wu.html_link % ("UNO_ui.py", "UNO")
        wu.html_content_index = 0
        index_html += wu.html_content("When only ONE card is in your hand, shout out UNO!")

        index_html += wu.html_sub_title % wu.html_link % ("http://www.oschina.net/p/shihira-htmlgen", "Shihira-Htmlgen")
        wu.html_content_index = 0
        index_html += wu.html_content("It's a static html page generator. Designed by Shihira Fung.")

        index_html += wu.html_footer % ""

        return [index_html]
