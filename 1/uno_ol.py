#### UNO_ui.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import shihira.webutil as wu
from shihira.database import *
from shihira.uno_base import cb, sp, Card

def app(environ, start_response):
        status = "200 OK" 
        headers = [("Content-type", "text/html")] 
        start_response(status, headers)

        args = wu.urlargs(environ)


        load_database()
        if "player" in args and "room" in args:
                #### Game 
                html = wu.html_header
                player = rescape(args["player"]).split()[0]
                room = rescape(args["room"]).split()[0]

                ongoing = None
                try: ongoing = int(get_var(cb("Ongoing", room)))
                except ValueError: # That var is an empty str
                        table = Card()
                        set_var(cb("Ongoing", room), "0")
                        set_var(cb("Table", room), str(table.color) + " " + table.number)
                        set_var(cb("CardEffect", room), "0")
                        set_var(cb("Direction", room), "1")
                        set_var(cb("Player", room), "")
                        set_var(cb("PlayerActive", room), "0");
                        set_var(cb("Message", room), "");

                player_list = get_var(cb("Player", room))
                if not cb(player, room) in player_list.split() and not ongoing:
                        set_var(cb("Player", room), player_list + " " + cb(player, room))
                        set_var(cb("Message", room), "<b>%s</b> comes in." % player)

                befr = ""
                befr += '<script>var org_url = "uno_ajax.py?player=%s&room=%s";</script>\n' % (player, room)
                befr += wu.html_jquery(environ)
                befr += '<link href="static/UNO/UNO.css" rel="stylesheet" type="text/css" />\n' \
                        '<script src="static/UNO/UNO.js"></script>\n'
                html = html.replace("<!-- link -->", befr)

                html += wu.html_sub_title % 'UNO %s<br/>&nbsp;<img src="static/loading.gif"' \
                        ' id="loading_pic"/>\n' % room
                wu.html_content_index = 0
                html += wu.html_content('<table width="100%" border="0"><tr>\n' \
                        '  <td id="table"></td>\n' \
                        '  <td width="100"><table id="player" width="100%" border="0"></table></td>\n' \
                        '</tr></table>\n')
                html += wu.html_content('').replace("<div", '<div id="message"')
                html += wu.html_sub_title % "Hand"
                wu.html_content_index = 0
                html += '<div id="quit" class="quit_btn">Quit</div>\n' \
                        '<div id="draw" class="oper_btn"></div>\n' \
                        '<div id="commit" class="oper_btn">Commit</div>\n' \
                        '<div id="hand"></div>\n'
                html += wu.html_footer % ""

        else:
                html = wu.html_header
                html += wu.html_sub_title % "Name, Please?"
                wu.html_content_index = 0
                html += '<div class="input-bar"><form>\n' \
                                '     <input class="submit-text" style="width:100px" type="text" name="room" value="Default-Room"/>\n' \
                        '     <input class="submit-text" type="text" name="player" value=""/>\n' \
                        '     <input class="submit-btn" type="submit" value="Play"/>\n' \
                        '</form></div>\n'
                html += wu.html_content("<em>(Case-sensitive)<br/><br/>Chinese character, A-Z, a-z, 0-9</em>")
                html += wu.html_sub_title % "Existed Rooms"
                wu.html_content_index = 0
                rooms = sql_query("select `variable`, `value` from `global_var` where `variable` like '%s';" % cb("Player", "%"))
                for room in rooms:
                        room_name = sp(room[0])[1]
                        room_players = room[1].split()
                        if not room_players:
                                sql = "delete from `global_var` where `variable` like '%s';" % cb("%", room_name)
                                sql_query(sql)
                                continue
                        room_players_str = ""
                        for room_player in room_players: room_players_str += "@%s " % sp(room_player)[0]
                        html += wu.html_content('<b>%s</b>: <span style="color:#333;">%s</span>\n' \
                                % (room_name, room_players_str))
                html += wu.html_footer % "The copyright on UNO<sup style=\"font-size:9px\">TM</sup> is owned by Mattel."

        unload_database()
        return [html]

