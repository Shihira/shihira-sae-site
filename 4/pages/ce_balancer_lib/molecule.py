import re

class group:
        def __parse(self, root, sub_group):
                units = []
                charge = 0
                unit_list = re.findall(r"([A-Z][a-z]?|#*)(-?\d*)", root)

                for unit, count in unit_list:
                        if not unit: continue
                        if not count: count = "1"
                        if count == "-": count = "-1"
                        if unit == "E": charge = int(count); continue
                        if unit.find("#") == -1:
                                units += [(unit, int(count))]
                        else:
                                units += [(group(sub_group[0]), int(count))]
                                del sub_group[0]

                return units, charge

        def from_string(self, grp_str):
                root = ""
                sub_group = []
                prths_stack = 0
                for c in grp_str:
                        if c == ")": prths_stack -= 1
                        if not ((c == "(" or c == ")") and not prths_stack):
                                if prths_stack:
                                        sub_group[len(sub_group) - 1] += c
                                        root += "#"
                                else: root += c
                        if c == "(":
                                if prths_stack == 0:
                                        sub_group += [""]
                                prths_stack += 1
                        
                self.__units, self.__charge = self.__parse(root, sub_group)

        def elements(self):
                counter = {}
                for unit, count in self.__units:
                        import types
                        if type(unit) == types.StringType:
                                if counter.has_key(unit): counter[unit] += count
                                else: counter[unit] = count
                        else:
                                subele = unit.elements()
                                for key in subele:
                                        if counter.has_key(key):
                                                counter[key] += subele[key] * count
                                        else: counter[key] = subele[key] * count
                if self.__charge: counter["E"] = self.__charge

                return counter


        def __init__(self, grp_str = ""):
                self.__units = []
                self.__charge = 0
                self.from_string(grp_str)

        def __str__(self):
                grp_str = ""
                for unit, count in self.__units:
                        # unit_str process
                        unit_str = str(unit)
                        import types
                        if type(unit) != types.StringType:
                                grp_str += "(" + unit_str + ")"
                        else: grp_str += unit_str

                        # count_str
                        count_str = str(count)
                        if count == 1: count_str = ""
                        grp_str += "<sub>%s</sub>" % count_str

                if not grp_str: grp_str = "e"
                # charge_str
                if self.__charge != 0:
                        charge_str = str(abs(self.__charge))
                        if abs(self.__charge) == 1: charge_str = ""
                        if self.__charge > 0: charge_str += "+"
                        else: charge_str += "-"
                        grp_str += "<sup>%s</sup>" % charge_str

                return grp_str

molecule = group

