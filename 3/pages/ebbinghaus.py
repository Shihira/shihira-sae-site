import web
from utility.database import db
from memorization.memterm import *
from memorization.dictionary import *

class page:
        def GET(self, mode):
                if mode == "list":
                        render = web.template.render("pages/templates/", base="ui_page")
                        terms = load_terms("Shihira")
                        terms.reverse()
                        return render.ebblist(terms)

                if mode == "query":
                        query = web.input()
                        web.header('Content-Type', 'text/plain')
                        ret_str = refer(query["word"].lower())
                        ret_str = " ".join(ret_str[1:])
                        return ret_str

                if mode == "review":
                        render = web.template.render("pages/templates/", base="ui_page")
                        return render.ebbreview()

                if mode == "next":
                        import json
                        query = web.input()
                        if "uuid" in query:
                                update_term(query["uuid"], query["state"] == "true")

                        web.header('Content-Type', 'text/plain')
                        temp = load_terms("Shihira")
                        prev_s = 0.0
                        terms = []
                        for term in temp:
                                prev_s += 1 - term.strength(False)
                                terms += [(prev_s, term)]

                        import random
                        #sel = random.choice(terms)[1]
                        val = random.uniform(0, prev_s)
                        ctg = random.randint(0, 2)
                        sel = terms[0][1]
                        for term in terms:
                                if term[0] > val:
                                        sel = term[1]
                                        break

                        jsdi = {}

                        if ctg == 0:
                                opt = [sel.term]
                                temp = random.sample(pick_similar(sel.term), 3)
                                for t in temp: opt += [t[0]]
                                random.shuffle(opt)

                                jsdi["uuid"] = sel.uuid
                                jsdi["tips"] = sel.tips
                                jsdi["options"] = opt
                                jsdi["correct"] = opt.index(sel.term)

                        if ctg == 1:
                                opt = [sel.tips]
                                temp = random.sample(pick_similar(sel.term), 3)
                                for t in temp: opt += [refer(t[0])[1]]
                                random.shuffle(opt)

                                jsdi["uuid"] = sel.uuid
                                jsdi["tips"] = sel.term
                                jsdi["options"] = opt
                                jsdi["correct"] = opt.index(sel.tips)

                        if ctg == 2:
                                jsdi["uuid"] = sel.uuid
                                jsdi["tips"] = sel.tips
                                jsdi["correct"] = sel.term

                        return json.dumps(jsdi)

        def POST(self, mode):
                query = web.input()
                if mode == "add":
                        term = memterm(query["term"], query["tips"])
                        insert_term("Shihira", term)
                if mode == "delete":
                        delete_term(query["uuid"])


