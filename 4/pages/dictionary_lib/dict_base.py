import struct
import os

class dict_base(object):

        def __init__(self):
                self.dict_pair = []
                self.packer = None

        def __del__(self):
                for pair in self.dict_pair:
                        pair[0].close()
                        pair[1].close()

        def _file_size(self, f):
                p = f.tell()
                f.seek(0, 2)
                s = f.tell()
                f.seek(p, 0)
                return s

        def _single_size(self, ifile):
                return self._file_size(ifile) / 4 - 1

        def _single_entry(self, ifile, dfile, index):
                if not self.packer:
                        self.packer = struct.Struct("II")
                        if self.packer.size != 8: 
                                self.packer = struct.Struct("LL")

                ifile.seek(index * 4)
                idata = ifile.read(8)

                pos = self.packer.unpack(idata)
                dfile.seek(pos[0])
                ddata = dfile.read(pos[1] - pos[0]) # get rid of \n
                ddata = ddata.strip().decode("utf8", "ignore")

                return self._split(ddata)

        def _split(self, entry):
                return entry.split("\t")

        def size(self):
                size_sum = 0
                for ifile, dfile in self.dict_pair:
                        size_sum += self._single_size(ifile)

                return size_sum

        def entry(self, index, no_expl = False):
                for ifile, dfile in self.dict_pair:
                        part_size = self._single_size(ifile)
                        if index >= part_size:
                                index -= part_size
                                continue
                        if no_expl:
                                return self._single_entry(
                                        ifile, dfile, index)[0]
                        else:
                                return self._single_entry(
                                        ifile, dfile, index)
                return None

        def load_pair(self, dict_name):
                path = os.path.abspath(os.path.dirname(__file__))
                ifile = open("%s/%s.index" % (path, dict_name), "rb")
                dfile = open("%s/%s.txt" % (path, dict_name), "rb")
                self.dict_pair += [(ifile, dfile)]

        def search(self, entry):
                entry = entry.upper()

                m = 0
                l = 0
                r = self.size()
                cur_data = ""

                while l < r and m != (l + r) / 2:
                        m = (l + r) / 2
                        cur_data = self.entry(m, True).upper()
                        if cur_data == entry:
                                break
                        if cur_data < entry:
                                l = m
                        if cur_data > entry:
                                r = m

                if cur_data == entry:
                        return self.entry(m)
                else: return None

        def fetch_all(self):
                ddata = []
                for ifile, dfile in self.dict_pair:
                        for line in dfile:
                                ddata += [self._split(line)[0]]

                return ddata
