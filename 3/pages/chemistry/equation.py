import re
import fractions
from numpy import linalg
from molecule import *

class equation:
        def get_matrix(self, augment = False):
                elements_list = []
                element_set = set()

                for mlc in self.molecules:
                        elements = mlc.elements()
                        element_set |= set(elements)
                        elements_list += [elements]

                lmatrix = []
                rmatrix = []
                amatrix = []
                for element in element_set:
                        line = []
                        for mlc in elements_list:
                                if mlc.has_key(element):
                                        line += [mlc[element]]
                                else: line += [0]
                        lmatrix += [line[:len(line) - 1]]
                        rmatrix += [line[len(line) - 1]]
                        amatrix += [line]

                if augment: return amatrix
                else: return lmatrix, rmatrix

        def balance(self):
                lmatrix, rmatrix = self.get_matrix()
                solution = list(linalg.solve(lmatrix, rmatrix)) + [-1]

                for i in range(len(solution)):
                        solution[i] = fractions.Fraction(solution[i]).limit_denominator()

                # find lcm of the denominator
                lcm = 1
                for i in solution:
                        den = i.denominator
                        lcm = lcm * den / fractions.gcd(lcm, den)
                for i in range(len(solution)):
                        solution[i] *= lcm
                        solution[i] = int(solution[i])

                self.stoich = solution

        def from_string(self, eqt_str):
                two_side = re.split("\s*[=|:]\s*", eqt_str)
                lmolecules = re.split("\s*[+|_]\s*", two_side[0])
                rmolecules = re.split("\s*[+|_]\s*", two_side[1])

                for mcl in (lmolecules + rmolecules):
                        self.molecules += [molecule(mcl)]
                self.stoich += [1] * len(lmolecules) + [-1] * len(rmolecules)

        def __init__(self, eqt_str = ""):
                self.molecules = []
                self.stoich = []

                self.from_string(eqt_str)

        def __str__(self):
                left_hand_side = []
                right_hand_side = []

                for stch, mlc in zip(self.stoich, self.molecules):
                        stch_str = str(abs(stch))
                        if abs(stch) == 1: stch_str = ""

                        if stch > 0: left_hand_side += [stch_str + str(mlc)]
                        if stch < 0: right_hand_side += [stch_str + str(mlc)]

                return " + ".join(left_hand_side) + " = " + " + ".join(right_hand_side)

