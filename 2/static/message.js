// Copyright 2013 Shihira Fung.
// E-mail: fengzhiping@hotmail.com
// Weibo: @Shihira
var browser_type = 0; // 0 : Mobile; 1 : PC/iPad

$(document).ready(function() {

function gen_url() {
        $("#loading_pic").css("display", "inline");
        var url = "message.py?ui=0&command=msg";
        var from = $("div.content_0").size() + $("div.content_1").size();
        url += "&from=" + from;
        
        return url;
}

function show_new(data) {
        $("#history").prepend(data);
        $("#loading_pic").css("display", "none");
        if(browser_type)
                $(".left-side").css("width", "400px");
}

function get_new() { $.get(gen_url(), show_new); }
function send_msg() { $.post(gen_url(), $("#msg_text").val(), show_new); }

$("#loading_pic").css("display", "none");
$("#msg_btn").click(send_msg);

if($(".page-title").width() > 400) browser_type = 1;
if(browser_type)
        $(".left-side").css("width", "400px");

setInterval(get_new, 10000);

});

