import web
from sae import create_wsgi_app

urls = (
"/", "pages.index.page",
"/root.txt", "pages.verify.page",
"/index/?", "pages.index.page",
"/ce_balancer/?", "pages.ce_balancer.page",
"/ebbinghaus/?", "pages.ebbinghaus.page",
"/ebbinghaus/(.*?)/?", "pages.ebbinghaus.page",
"/dictionary/(.*?)/(.*?)/?", "pages.dictionary.page",
"/article/(.*?)/?", "pages.article.page",
"/uno/?", "pages.uno.page",
"/uno/(.*?)/?", "pages.uno.page",
)


###########################################################
# Need not modify

app = web.application(urls, globals())
application = create_wsgi_app(app.wsgifunc())

