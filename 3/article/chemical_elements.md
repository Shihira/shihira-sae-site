<h2>Chemical Elements Reference</h2>
<script>
$(document).ready(function(){
        $("table tbody tr:even").css("background-color", "#CCC")
});
</script>
<ul><li>
<table cellpadding="0" width="506" style="width:100%; text-align:center;">
  <thead>
    <tr>        <th>序</th>	<th>名</th>	<th>符</th>	<th>族</th>	<th>周</th>	<th>质</th>	<th>状</th></tr>
  </thead>
  <tbody>
      <tr> 	<td>1</td>	<td>氢</td>	<td>H</td>	<td>1</td>	<td>1</td>	<td>1.00</td>	<td>g</td> </tr>
      <tr> 	<td>2</td>	<td>氦</td>	<td>He</td>	<td>18</td>	<td>1</td>	<td>4.00</td>	<td>g</td> </tr>
      <tr> 	<td>3</td>	<td>锂</td>	<td>Li</td>	<td>1</td>	<td>2</td>	<td>6.94</td>	<td>s</td> </tr>
      <tr> 	<td>4</td>	<td>铍</td>	<td>Be</td>	<td>2</td>	<td>2</td>	<td>9.01</td>	<td>s</td> </tr>
      <tr> 	<td>5</td>	<td>硼</td>	<td>B</td>	<td>13</td>	<td>2</td>	<td>10.81</td>	<td>s</td> </tr>
      <tr> 	<td>6</td>	<td>碳</td>	<td>C</td>	<td>14</td>	<td>2</td>	<td>12.01</td>	<td>s</td> </tr>
      <tr> 	<td>7</td>	<td>氮</td>	<td>N</td>	<td>15</td>	<td>2</td>	<td>14.00</td>	<td>g</td> </tr>
      <tr> 	<td>8</td>	<td>氧</td>	<td>O</td>	<td>16</td>	<td>2</td>	<td>15.99</td>	<td>g</td> </tr>
      <tr> 	<td>9</td>	<td>氟</td>	<td>F</td>	<td>17</td>	<td>2</td>	<td>18.99</td>	<td>g</td> </tr>
      <tr> 	<td>10</td>	<td>氖</td>	<td>Ne</td>	<td>18</td>	<td>2</td>	<td>20.17</td>	<td>g</td> </tr>
      <tr> 	<td>11</td>	<td>钠</td>	<td>Na</td>	<td>1</td>	<td>3</td>	<td>22.98</td>	<td>s</td> </tr>
      <tr> 	<td>12</td>	<td>镁</td>	<td>Mg</td>	<td>2</td>	<td>3</td>	<td>24.30</td>	<td>s</td> </tr>
      <tr> 	<td>13</td>	<td>铝</td>	<td>Al</td>	<td>13</td>	<td>3</td>	<td>26.98</td>	<td>s</td> </tr>
      <tr> 	<td>14</td>	<td>硅</td>	<td>Si</td>	<td>14</td>	<td>3</td>	<td>28.08</td>	<td>s</td> </tr>
      <tr> 	<td>15</td>	<td>磷</td>	<td>P</td>	<td>15</td>	<td>3</td>	<td>30.97</td>	<td>s</td> </tr>
      <tr> 	<td>16</td>	<td>硫</td>	<td>S</td>	<td>16</td>	<td>3</td>	<td>32.06</td>	<td>s</td> </tr>
      <tr> 	<td>17</td>	<td>氯</td>	<td>Cl</td>	<td>17</td>	<td>3</td>	<td>35.45</td>	<td>g</td> </tr>
      <tr> 	<td>18</td>	<td>氩</td>	<td>Ar</td>	<td>18</td>	<td>3</td>	<td>39.94</td>	<td>g</td> </tr>
      <tr> 	<td>19</td>	<td>钾</td>	<td>K</td>	<td>1</td>	<td>4</td>	<td>39.09</td>	<td>s</td> </tr>
      <tr> 	<td>20</td>	<td>钙</td>	<td>Ca</td>	<td>2</td>	<td>4</td>	<td>40.07</td>	<td>s</td> </tr>
      <tr> 	<td>21</td>	<td>钪</td>	<td>Sc</td>	<td>3</td>	<td>4</td>	<td>44.95</td>	<td>s</td> </tr>
      <tr> 	<td>22</td>	<td>钛</td>	<td>Ti</td>	<td>4</td>	<td>4</td>	<td>47.86</td>	<td>s</td> </tr>
      <tr> 	<td>23</td>	<td>钒</td>	<td>V</td>	<td>5</td>	<td>4</td>	<td>50.94</td>	<td>s</td> </tr>
      <tr> 	<td>24</td>	<td>铬</td>	<td>Cr</td>	<td>6</td>	<td>4</td>	<td>51.99</td>	<td>s</td> </tr>
      <tr> 	<td>25</td>	<td>锰</td>	<td>Mn</td>	<td>7</td>	<td>4</td>	<td>54.93</td>	<td>s</td> </tr>
      <tr> 	<td>26</td>	<td>铁</td>	<td>Fe</td>	<td>8</td>	<td>4</td>	<td>55.84</td>	<td>s</td> </tr>
      <tr> 	<td>27</td>	<td>钴</td>	<td>Co</td>	<td>9</td>	<td>4</td>	<td>58.93</td>	<td>s</td> </tr>
      <tr> 	<td>28</td>	<td>镍</td>	<td>Ni</td>	<td>10</td>	<td>4</td>	<td>58.69</td>	<td>s</td> </tr>
      <tr> 	<td>29</td>	<td>铜</td>	<td>Cu</td>	<td>11</td>	<td>4</td>	<td>63.54</td>	<td>s</td> </tr>
      <tr> 	<td>30</td>	<td>锌</td>	<td>Zn</td>	<td>12</td>	<td>4</td>	<td>65.38</td>	<td>s</td> </tr>
      <tr> 	<td>31</td>	<td>镓</td>	<td>Ga</td>	<td>13</td>	<td>4</td>	<td>69.72</td>	<td>s</td> </tr>
      <tr> 	<td>32</td>	<td>锗</td>	<td>Ge</td>	<td>14</td>	<td>4</td>	<td>72.64</td>	<td>s</td> </tr>
      <tr> 	<td>33</td>	<td>砷</td>	<td>As</td>	<td>15</td>	<td>4</td>	<td>74.92</td>	<td>s</td> </tr>
      <tr> 	<td>34</td>	<td>硒</td>	<td>Se</td>	<td>16</td>	<td>4</td>	<td>78.96</td>	<td>s</td> </tr>
      <tr> 	<td>35</td>	<td>溴</td>	<td>Br</td>	<td>17</td>	<td>4</td>	<td>79.90</td>	<td>l</td> </tr>
      <tr> 	<td>36</td>	<td>氪</td>	<td>Kr</td>	<td>18</td>	<td>4</td>	<td>83.79</td>	<td>g</td> </tr>
      <tr> 	<td>37</td>	<td>铷</td>	<td>Rb</td>	<td>1</td>	<td>5</td>	<td>85.46</td>	<td>s</td> </tr>
      <tr> 	<td>38</td>	<td>锶</td>	<td>Sr</td>	<td>2</td>	<td>5</td>	<td>87.62</td>	<td>s</td> </tr>
      <tr> 	<td>39</td>	<td>钇</td>	<td>Y</td>	<td>3</td>	<td>5</td>	<td>88.90</td>	<td>s</td> </tr>
      <tr> 	<td>40</td>	<td>锆</td>	<td>Zr</td>	<td>4</td>	<td>5</td>	<td>91.22</td>	<td>s</td> </tr>
      <tr> 	<td>41</td>	<td>铌</td>	<td>Nb</td>	<td>5</td>	<td>5</td>	<td>92.90</td>	<td>s</td> </tr>
      <tr> 	<td>42</td>	<td>钼</td>	<td>Mo</td>	<td>6</td>	<td>5</td>	<td>95.96</td>	<td>s</td> </tr>
      <tr> 	<td>43</td>	<td>锝</td>	<td>Tc</td>	<td>7</td>	<td>5</td>	<td>98.90</td>	<td>s</td> </tr>
      <tr> 	<td>44</td>	<td>钌</td>	<td>Ru</td>	<td>8</td>	<td>5</td>	<td>101.07</td>	<td>s</td> </tr>
      <tr> 	<td>45</td>	<td>铑</td>	<td>Rh</td>	<td>9</td>	<td>5</td>	<td>102.90</td>	<td>s</td> </tr>
      <tr> 	<td>46</td>	<td>钯</td>	<td>Pd</td>	<td>10</td>	<td>5</td>	<td>106.42</td>	<td>s</td> </tr>
      <tr> 	<td>47</td>	<td>银</td>	<td>Ag</td>	<td>11</td>	<td>5</td>	<td>107.86</td>	<td>s</td> </tr>
      <tr> 	<td>48</td>	<td>镉</td>	<td>Cd</td>	<td>12</td>	<td>5</td>	<td>112.41</td>	<td>s</td> </tr>
      <tr> 	<td>49</td>	<td>铟</td>	<td>In</td>	<td>13</td>	<td>5</td>	<td>114.81</td>	<td>s</td> </tr>
      <tr> 	<td>50</td>	<td>锡</td>	<td>Sn</td>	<td>14</td>	<td>5</td>	<td>118.71</td>	<td>s</td> </tr>
      <tr> 	<td>51</td>	<td>锑</td>	<td>Sb</td>	<td>15</td>	<td>5</td>	<td>121.76</td>	<td>s</td> </tr>
      <tr> 	<td>52</td>	<td>碲</td>	<td>Te</td>	<td>16</td>	<td>5</td>	<td>127.60</td>	<td>s</td> </tr>
      <tr> 	<td>53</td>	<td>碘</td>	<td>I</td>	<td>17</td>	<td>5</td>	<td>126.90</td>	<td>s</td> </tr>
      <tr> 	<td>54</td>	<td>氙</td>	<td>Xe</td>	<td>18</td>	<td>5</td>	<td>131.29</td>	<td>g</td> </tr>
      <tr> 	<td>55</td>	<td>铯</td>	<td>Cs</td>	<td>1</td>	<td>6</td>	<td>132.90</td>	<td>s</td> </tr>
      <tr> 	<td>56</td>	<td>钡</td>	<td>Ba</td>	<td>2</td>	<td>6</td>	<td>137.32</td>	<td>s</td> </tr>
      <tr> 	<td>57</td>	<td>镧</td>	<td>La</td>	<td>3</td>	<td>6</td>	<td>138.90</td>	<td>s</td> </tr>
      <tr> 	<td>58</td>	<td>铈</td>	<td>Ce</td>	<td>3</td>	<td>6</td>	<td>140.11</td>	<td>s</td> </tr>
      <tr> 	<td>59</td>	<td>镨</td>	<td>Pr</td>	<td>3</td>	<td>6</td>	<td>140.90</td>	<td>s</td> </tr>
      <tr> 	<td>60</td>	<td>钕</td>	<td>Nd</td>	<td>3</td>	<td>6</td>	<td>144.24</td>	<td>s</td> </tr>
      <tr> 	<td>61</td>	<td>钷</td>	<td>Pm</td>	<td>3</td>	<td>6</td>	<td>144.9</td>	<td>s</td> </tr>
      <tr> 	<td>62</td>	<td>钐</td>	<td>Sm</td>	<td>3</td>	<td>6</td>	<td>150.36</td>	<td>s</td> </tr>
      <tr> 	<td>63</td>	<td>铕</td>	<td>Eu</td>	<td>3</td>	<td>6</td>	<td>151.96</td>	<td>s</td> </tr>
      <tr> 	<td>64</td>	<td>钆</td>	<td>Gd</td>	<td>3</td>	<td>6</td>	<td>157.25</td>	<td>s</td> </tr>
      <tr> 	<td>65</td>	<td>铽</td>	<td>Tb</td>	<td>3</td>	<td>6</td>	<td>158.92</td>	<td>s</td> </tr>
      <tr> 	<td>66</td>	<td>镝</td>	<td>Dy</td>	<td>3</td>	<td>6</td>	<td>162.50</td>	<td>s</td> </tr>
      <tr> 	<td>67</td>	<td>钬</td>	<td>Ho</td>	<td>3</td>	<td>6</td>	<td>164.93</td>	<td>s</td> </tr>
      <tr> 	<td>68</td>	<td>铒</td>	<td>Er</td>	<td>3</td>	<td>6</td>	<td>167.25</td>	<td>s</td> </tr>
      <tr> 	<td>69</td>	<td>铥</td>	<td>Tm</td>	<td>3</td>	<td>6</td>	<td>168.93</td>	<td>s</td> </tr>
      <tr> 	<td>70</td>	<td>镱</td>	<td>Yb</td>	<td>3</td>	<td>6</td>	<td>173.05</td>	<td>s</td> </tr>
      <tr> 	<td>71</td>	<td>镥</td>	<td>Lu</td>	<td>3</td>	<td>6</td>	<td>174.96</td>	<td>s</td> </tr>
      <tr> 	<td>72</td>	<td>铪</td>	<td>Hf</td>	<td>4</td>	<td>6</td>	<td>178.49</td>	<td>s</td> </tr>
      <tr> 	<td>73</td>	<td>钽</td>	<td>Ta</td>	<td>5</td>	<td>6</td>	<td>180.94</td>	<td>s</td> </tr>
      <tr> 	<td>74</td>	<td>钨</td>	<td>W</td>	<td>6</td>	<td>6</td>	<td>183.84</td>	<td>s</td> </tr>
      <tr> 	<td>75</td>	<td>铼</td>	<td>Re</td>	<td>7</td>	<td>6</td>	<td>186.20</td>	<td>s</td> </tr>
      <tr> 	<td>76</td>	<td>锇</td>	<td>Os</td>	<td>8</td>	<td>6</td>	<td>190.23</td>	<td>s</td> </tr>
      <tr> 	<td>77</td>	<td>铱</td>	<td>Ir</td>	<td>9</td>	<td>6</td>	<td>192.21</td>	<td>s</td> </tr>
      <tr> 	<td>78</td>	<td>铂</td>	<td>Pt</td>	<td>10</td>	<td>6</td>	<td>195.08</td>	<td>s</td> </tr>
      <tr> 	<td>79</td>	<td>金</td>	<td>Au</td>	<td>11</td>	<td>6</td>	<td>196.96</td>	<td>s</td> </tr>
      <tr> 	<td>80</td>	<td>汞</td>	<td>Hg</td>	<td>12</td>	<td>6</td>	<td>200.59</td>	<td>l</td> </tr>
      <tr> 	<td>81</td>	<td>铊</td>	<td>Tl</td>	<td>13</td>	<td>6</td>	<td>204.38</td>	<td>s</td> </tr>
      <tr> 	<td>82</td>	<td>铅</td>	<td>Pb</td>	<td>14</td>	<td>6</td>	<td>207.2</td>	<td>s</td> </tr>
      <tr> 	<td>83</td>	<td>铋</td>	<td>Bi</td>	<td>15</td>	<td>6</td>	<td>208.98</td>	<td>s</td> </tr>
      <tr> 	<td>84</td>	<td>钋</td>	<td>Po </td>	<td>16</td>	<td>6</td>	<td>208.98</td>	<td>s</td> </tr>
      <tr> 	<td>85</td>	<td>砹</td>	<td>At</td>	<td>17</td>	<td>6</td>	<td>209.98</td>	<td>s</td> </tr>
      <tr> 	<td>86</td>	<td>氡</td>	<td>Rn</td>	<td>18</td>	<td>6</td>	<td>222.01</td>	<td>g</td> </tr>
      <tr> 	<td>87</td>	<td>钫</td>	<td>Fr</td>	<td>1</td>	<td>7</td>	<td>223.01</td>	<td>s</td> </tr>
      <tr> 	<td>88</td>	<td>镭</td>	<td>Ra</td>	<td>2</td>	<td>7</td>	<td>226.02</td>	<td>s</td> </tr>
      <tr> 	<td>89</td>	<td>锕</td>	<td>Ac</td>	<td>3</td>	<td>7</td>	<td>227.02</td>	<td>s</td> </tr>
      <tr> 	<td>90</td>	<td>钍</td>	<td>Th</td>	<td>3</td>	<td>7</td>	<td>232.03</td>	<td>s</td> </tr>
      <tr> 	<td>91</td>	<td>镤</td>	<td>Pa</td>	<td>3</td>	<td>7</td>	<td>231.03</td>	<td>s</td> </tr>
      <tr> 	<td>92</td>	<td>铀</td>	<td>U</td>	<td>3</td>	<td>7</td>	<td>238.02</td>	<td>s</td> </tr>
      <tr> 	<td>93</td>	<td>镎</td>	<td>Np</td>	<td>3</td>	<td>7</td>	<td>237.04</td>	<td>s</td> </tr>
      <tr> 	<td>94</td>	<td>钚</td>	<td>Pu</td>	<td>3</td>	<td>7</td>	<td>239.06</td>	<td>s</td> </tr>
      <tr> 	<td>95</td>	<td>镅</td>	<td>Am</td>	<td>3</td>	<td>7</td>	<td>243.06</td>	<td>s</td> </tr>
      <tr> 	<td>96</td>	<td>锔</td>	<td>Cm</td>	<td>3</td>	<td>7</td>	<td>247.07</td>	<td>s</td> </tr>
      <tr> 	<td>97</td>	<td>锫</td>	<td>Bk</td>	<td>3</td>	<td>7</td>	<td>247.07</td>	<td>s</td> </tr>
      <tr> 	<td>98</td>	<td>锎</td>	<td>Cf</td>	<td>3</td>	<td>7</td>	<td>251.07</td>	<td>s</td> </tr>
      <tr> 	<td>99</td>	<td>锿</td>	<td>Es</td>	<td>3</td>	<td>7</td>	<td>252.08</td>	<td>s</td> </tr>
      <tr> 	<td>100</td>	<td>镄</td>	<td>Fm</td>	<td>3</td>	<td>7</td>	<td>257.05</td>	<td>s</td> </tr>
      <tr> 	<td>101</td>	<td>钔</td>	<td>Md</td>	<td>3</td>	<td>7</td>	<td>258.09</td>	<td>s</td> </tr>
      <tr> 	<td>102</td>	<td>锘</td>	<td>No</td>	<td>3</td>	<td>7</td>	<td>259.10</td>	<td>s</td> </tr>
      <tr> 	<td>103</td>	<td>铹</td>	<td>Lr</td>	<td>3</td>	<td>7</td>	<td>262.10</td>	<td>s</td> </tr>
      <tr> 	<td>104</td>	<td>金卢</td>	<td>Rf</td>	<td>4</td>	<td>7</td>	<td>261.10</td>	<td></td> </tr>
      <tr> 	<td>105</td>	<td>金杜</td>	<td>Db</td>	<td>5</td>	<td>7</td>	<td>262.11</td>	<td></td> </tr>
      <tr> 	<td>106</td>	<td>金喜</td>	<td>Sg</td>	<td>6</td>	<td>7</td>	<td>266.12</td>	<td></td> </tr>
      <tr> 	<td>107</td>	<td>金波</td>	<td>Bh</td>	<td>7</td>	<td>7</td>	<td>264.12</td>	<td></td> </tr>
      <tr> 	<td>108</td>	<td>金黑</td>	<td>Hs</td>	<td>8</td>	<td>7</td>	<td>[277]</td>	<td></td> </tr>
      <tr> 	<td>109</td>	<td>金麦</td>	<td>Mt</td>	<td>9</td>	<td>7</td>	<td>[268]</td>	<td></td> </tr>
      <tr> 	<td>110</td>	<td>金达</td>	<td>Ds</td>	<td>10</td>	<td>7</td>	<td>[271]</td>	<td></td> </tr>
      <tr> 	<td>111</td>	<td>金仑</td>	<td>Rg</td>	<td>11</td>	<td>7</td>	<td>[272]</td>	<td></td> </tr>
      <tr> 	<td>112</td>	<td>金哥</td>	<td>Cn</td>	<td>12</td>	<td>7</td>	<td>[285]</td>	<td></td> </tr>
      <tr> 	<td>113</td>	<td></td>	<td>Uut</td>	<td>13</td>	<td>7</td>	<td>[284]</td>	<td></td> </tr>
      <tr> 	<td>114</td>	<td>金夫</td>	<td>Fl</td>	<td>14</td>	<td>7</td>	<td>[289]</td>	<td></td> </tr>
      <tr> 	<td>115</td>	<td></td>	<td>Uup</td>	<td>15</td>	<td>7</td>	<td>[288]</td>	<td></td> </tr>
      <tr> 	<td>116</td>	<td>金立</td>	<td>Lv</td>	<td>16</td>	<td>7</td>	<td>[292]</td>	<td></td> </tr>
      <tr> 	<td>117</td>	<td></td>	<td>Uus</td>	<td>17</td>	<td>7</td>	<td>[295]</td>	<td></td> </tr>
      <tr> 	<td>118</td>	<td></td>	<td>Uuo</td>	<td>18</td>	<td>7</td>	<td>[293]</td>	<td></td> </tr>
  </tbody>
</table>
</ul></li>
