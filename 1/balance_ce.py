#### balance_ce.py
# Copyright 2013 Shihira Fung.
# E-mail: fengzhiping@hotmail.com

from shihira.balance_ce import balance_ce
from shihira.webutil import urlargs
import shihira.webutil as wu

def app(environ, start_response):
        status = "200 OK" 
        headers = [("Content-type", "text/html")] 

        html  = wu.html_header
        html += wu.html_sub_title % wu.html_link % ("balance_ce.py", "Balance Chemical Equation")
        html += '<div class="input-bar">\n' \
                '  <form>\n' \
                '    <input class="submit-text" type="text" name="ce" value="%s"/>\n' \
                '    <input class="submit-btn" type="submit" value="Run"/>\n' \
                '  </form>\n' \
                '</div>\n'
        html += wu.html_sub_title % "%s" + "%s" 
        html += wu.html_footer % ( wu.md2html( \
                "The result above is only for reference.\n\n" \
                "__Help:__  \n" \
                "\* Refer to examples on the [start page](balance_ce.py).  \n" \
                "\* Use E as ion charges.  \n" \
                "\* Parentheses are now available.\n\n"))

        wu.html_content_index = 0
        i_e = wu.html_content("CH3CHO + Ag(NH3)2OH = CH3COONH4 + Ag + NH3 + H2O")
        i_e += wu.html_content("CrO2E- + OHE- + H2O2 = CrO4E-2 + H2O")
        i_e += wu.html_content("Al+NaOH+H2O=NaAlO2+H2")
        i_e += wu.html_content("Cu_H2SO4:CuSO4_SO2_H2O")

        args = urlargs(environ)

        wu.html_content_index = 0
        if "ce" in args:
                error, balanced_ce = balance_ce(args["ce"])
                if not error:
                        html = html % (args["ce"], "Result", wu.html_content(balanced_ce))
                else:
                        errmsg = 'Please Check your CE or contact ' \
                                 '<a href="http://www.weibo.com/shihira">@Shihira</a>. <br /><br />' + balanced_ce
                        html = html % (args["ce"], '<span style="color:#F00;">Error</span>', wu.html_content(errmsg))
        else:
                html = html % ("", "Examples", i_e)
        start_response(status, headers)
        return [html]
        
