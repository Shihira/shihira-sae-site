from dict_base import dict_base

class egstnc(dict_base):
        def entry(self, index, no_expl = False):
                father = super(egstnc, self)

                import re
                ret = father.entry(index, no_expl)

                if not no_expl:
                        ret[1] = ret[1].split("\\n")
                        for i, egs in enumerate(ret[1]):
                                ret[1][i] = egs.split("\\t")
                return ret

        def __init__(self):
                father = super(egstnc, self)

                father.__init__()
                self.load_pair("egstnc")

#import random
#print egstnc().entry(random.randint(0, 7000))
