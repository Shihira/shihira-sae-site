#### index.wsgi
# Copyright 2013 Shihira Fung.
# E-mail: fengzhiping@hotmail.com

from sae import create_wsgi_app

import os
import shihira.webutil as wu

def index_app(environ, start_response):
        url = environ["PATH_INFO"]
        if url == "/": url = "/start.py"
        fn, ext = os.path.splitext(url)

        if ext == ".py":
                mdl = fn.replace("/", ".")[1:]
                exec "from " + mdl + " import app"
                environ["SCRIPT_NAME"] = url
                return app(environ, start_response)

application = create_wsgi_app(index_app)
