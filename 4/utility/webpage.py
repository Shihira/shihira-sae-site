def md2html(finput, encoding="utf-8"):
        import markdown
        import re
        import urllib
        
        # handling equations
        frag = re.split(r"(?<!\\)\$", finput)
        url_prefix = '![%s](http://latex.codecogs.com/png.latex?%s)'
        for i in range(0, len(frag)):
                if i % 2: frag[i] = url_prefix % (frag[i], urllib.quote(frag[i]))
        text = "".join(frag)
        text = text.replace("\\$", "$")
        
        try: text = text.decode("utf-8")
        except:
                text = text.decode("gbk")
                open(fname,"w").write(finput.decode("gbk").encode("utf8"))
                
        html = markdown.markdown(text)
        html = html.encode("utf-8")
        return html
