#### index.wsgi
# Copyright 2013 Shihira Fung.
# E-mail: fengzhiping@hotmail.com

from sae import create_wsgi_app

import os
import shihira.webutil as wu

def index_app(environ, start_response):
        url = environ["PATH_INFO"]
        if url == "/": url = "/start.py"
        fn, ext = os.path.splitext(url)

        if ext == ".py" or ext == ".iamshihira":
                try:
                        mdl = fn.replace("/", ".")[1:]
                        exec "from " + mdl + " import app"
                        environ["SCRIPT_NAME"] = url
                        return app(environ, start_response)

                except Exception, e:
                        error_msg = ""

                        import traceback
                        print e
                        print traceback.format_exc()

                        if ext == ".iamshihira":
                                error_msg += str(e) + str(traceback.format_exc())

                        try: start_response("500 Internal Server Error", [])
                        except: return [error_msg]
                        return [error_msg]

        start_response("404 Not Found", [])
        return ["404 Not Found"]

application = create_wsgi_app(index_app)
