import web
from utility.database import db
from uuid import uuid1
from time import time as time1

class ebbitem:
        def __init__(self, uuid, gid, item, content, time, strength):
                self.uuid     = uuid
                self.gid      = gid
                self.item     = item
                self.content  = content
                self.time     = time
                self.strength = strength

        def dump(self):
                ret = db.select("ebbinghaus_items",
                        where = web.db.sqlwhere({"uuid": self.uuid}))

                if ret:
                        db.update("ebbinghaus_items",
                                where    = "uuid = '%s'" % self.uuid,
                                uuid     = self.uuid,
                                gid      = self.gid,
                                item     = self.item,
                                content  = self.content,
                                time     = self.time,
                                strength = self.strength)
                else:
                        db.insert("ebbinghaus_items",
                                uuid     = self.uuid,
                                gid      = self.gid,
                                item     = self.item,
                                content  = self.content,
                                time     = self.time,
                                strength = self.strength)

        def strength_coef(self):
                import math

                t = abs(time1() - self.time)
                s = self.strength # alias

                limit_time = 3600 * 36
                hftime = [1200, 120, 300, 600]

                # expire policy:
                # less than 36 hours: one-time-pass policy
                # more than 36 hours: drawback to the floor of 4
                if t > hftime[0]:
                        if t < limit_time:
                                self.strength += 3 - (s % 4)
                        if t >= limit_time:
                                self.strength -= s % 4
                        self.dump()

                # periodic repeat time - no math calculation
                if t > hftime[s % 4] and t < hftime[0]:
                        return -512 - (t - hftime[s % 4])

                # these are maths below
                s -= 3
                v1 = math.e ** ( - (t ** 0.1 / s))
                v2 = math.e ** ( - (t / (2000 * s ** 2)))

                if v1 > v2: return v1
                else: return v2

def from_text(gid, item, content = ""):
        uuid = uuid1()
        time = time1()
        strength = 4

        return ebbitem(uuid, gid, item, content, time, strength)

def from_iid(uuid):
        result = db.select("ebbinghaus_items",
                what = "`uuid`, `gid`, `item`, `content`, `time`, `strength`",
                where = "`uuid` = '%s'" % uuid)[0]
        return ebbitem(result.uuid, result.gid,
            result.item, result.content,
            result.time, result.strength)

def del_iid(uuid):
        db.delete("ebbinghaus_items",
                where = "`uuid` = '%s'" % uuid)

def strength_revise():
        db.update("ebbinghaus_items",
                where = "strength < 4",
                strength = 4)
                
