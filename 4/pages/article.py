import web
import utility.webpage as wp

class page:
        def GET(self, article):
                render = web.template.render("templates/", base="framework")
                file_md = open("article/%s.md" % article, "r").read()
                if file_md[0] != "<": return render.article(wp.md2html(file_md))
                else: return render.article(file_md)
