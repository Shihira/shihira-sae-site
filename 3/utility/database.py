from sae.const import *
import web

db = web.database(dbn = 'mysql',
        user = MYSQL_USER,
        pw = MYSQL_PASS,
        db = MYSQL_DB,
        host = MYSQL_HOST,
        port = int(MYSQL_PORT))

