$(document).ready(function() {
        var color = ["#FA5858", "#31B404", "#00BFFF", "#FFDA00"];
        var overcolor = ["#F78181", "#81F781", "#81DAF5", "#F3F781"];
        var outline = {"sel": "#045FB4 solid 3px", "nsel": "#AAA solid 1px"};

        var main_card = "";
        $.fn.play_card = function() {
                var t = $(this).offset().top;
                var l = $(this).offset().left;
                if($(this).css("position") != "absolute")
                        $(this).after('<div class="hand-card linear-element occu"' +
                                ' style="visibility: hidden;"></div>');

                $(this).css("position", "absolute");
                $(this).css("top", t - 10 + "px");
                $(this).css("left", l + "px");

                $(this).animate({
                        top: $("#table-card").offset().top - 5,
                        left: $("#table-card").offset().left - 5,
                        width: $("#table-card").css("width"),
                        height: $("#table-card").css("height"),
                }, 1000, function() {
                        if($(this).attr("uuid") == main_card) {
                                //$("#table-card").css("background-image", $(this).css("background-image"));
                                $("#table-card").attr("pattern", $(this).attr("pattern"));
                                $("#table-card").attr("color", $(this).attr("color"));
                                $("#table-card").make_card();
                                main_card = "";
                        }
                        $(this).remove();
                        $(".occu").remove();
                });
        }

        $.fn.make_card = function() {
                $(this).addClass("hand-card linear-element img-rounded");
                $(this).attr("sel", "nsel");
                $(this).css("outline", outline["nsel"]);
                var col = color[$(this).attr("color")];
                var pat = $(this).attr("pattern");
                $(this).css("background-image", "url("+png_url[pat]+")");
                $(this).css("background-color", col);
        }

        function play_clicked() {
                $("[uuid="+main_card+"]").css("z-index", "1024");
                $("[sel=sel]").each(function(){$(this).play_card();});
        }

        $("#hand-cards .hand-card").click(function() {
                var sel = $(this).attr("sel");
                if(!sel || sel == "nsel") sel = "sel";
                else sel = "nsel";
                $(this).attr("sel", sel);
                $(this).css("outline", outline[sel]);
        });

        $(".hand-card, #table-card").each(function(){$(this).make_card();});

        $("#btn-confirm").popover({
                html: true,
                content: function() { return $("#color-popover").html(); }, title:"Select the main color",
                placement: "top"
        });

        $("#btn-confirm").click(function() {
                //count colors
                var color_num = 0;
                var color_enable = [0, 0, 0, 0];
                $("[sel=sel]").each(function() {
                        color_enable[parseInt($(this).attr("color"))] = 1; });
                for(i in color_enable) if(color_enable[i] == 1) color_num += 1;

                //if the count of color less than one, play the card directly
                if(color_num <= 1) {
                        main_card = $("[sel=sel]:last").attr("uuid");
                        $("#btn-confirm").popover("hide");
                        play_clicked();
                        return;
                }

                //if the count of color more than one, init color board
                for(i in color) {
                        var cur = $(".sel-color[color="+i+"]")
                        if(color_enable[i]) {
                                cur.css("background-color", color[i]);
                                cur.mouseenter(function(){$(this).css("background-color", overcolor[parseInt($(this).attr("color"))]);});
                                cur.mouseleave(function(){$(this).css("background-color", color[parseInt($(this).attr("color"))]);});
                                cur.click(function() {
                                        main_card = $("[sel=sel][color="+$(this).attr("color")+"]").attr("uuid");
                                        $("#btn-confirm").popover("hide");
                                        play_clicked();
                                });
                        }
                        else if(!color_enable[i]) {
                                cur.unbind();
                                cur.css("background-color", "#EEE");
                        }
                }

        });

        function others_play(player) {
                var tr = $("tr:eq("+player+")");
                var t = tr.offset().top;
                var l = tr.offset().left;

                $("body").append('<div class="hand-card linear-element img-rounded"' +
                        ' style="position:absolute;" id="effect" color="1" pattern="x" uuid="unique-12138"></div>');
                var ec = $("#effect");
                ec.make_card();
                ec.css("top", t + "px");
                ec.css("left", l + "px");
                main_card = "unique-12138";
                ec.play_card();
        }

        $("#btn-check").click(function() {
                $("[sel=sel]").trigger("click");
        });

        $("tr").click(function() {
                var i = $(this).index("tr");
                if(i != 0) others_play(i);
        });
});
