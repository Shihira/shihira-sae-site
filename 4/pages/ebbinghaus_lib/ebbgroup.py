from utility.database import db
from uuid import uuid1
from ebbitem import ebbitem

class ebbgroup:
        def __init__(self, uuid, uid, name):
                self.uuid     = uuid
                self.uid      = uid
                self.name    = name

        def dump(self):
                db.insert("ebbinghaus_groups", 
                           uuid = self.uuid,
                           uid = self.uid,
                           name = self.name)

        def get_items_digest(self):
                results = db.select("ebbinghaus_items",
                        what = "`item`",
                        where = "`gid` = '%s'" % self.uuid,
                        limit = 3)
                items = []

                if not results:
                        return "empty ~ \xe2\x95\xae(\xe2\x95\xaf_\xe2\x95\xb0)\xe2\x95\xad"

                for i in range(0, len(results)):
                        items += [results[i].item]
                return ", ".join(items) + ", ..."

        def get_items(self):
                results = db.select("ebbinghaus_items",
                        what = "`uuid`, `gid`, `item`, `content`, `time`, `strength`",
                        where = "`gid` = '%s'" % self.uuid,
                        order = "time DESC")

                items = []
                for result in results:
                        items += [ebbitem(result.uuid, result.gid,
                                       result.item, result.content,
                                       result.time, result.strength)]
                return items

        def get_items_count(self):
                result = db.select("ebbinghaus_items",
                        what = "count(*)",
                        where = "`gid` = '%s'" % str(self.uuid))
                return result[0]["count(*)"]


def from_text(uid, name):
        uuid = uuid1()

        return ebbgroup(uuid, uid, name)

def from_gid(uuid):
        result = db.select("ebbinghaus_groups",
                what = "`uuid`, `uid`, `name`",
                where = "`uuid` = '%s'" % uuid)[0]

        return ebbgroup(result.uuid, result.uid,
                result.name)

def from_uid(uuid):
        results = db.select("ebbinghaus_groups",
                what = "`uuid`, `uid`, `name`",
                where = "`uid` = '%s'" % uuid)
        names = []
        for result in results:
                names += [ebbgroup(result.uuid, result.uid, result.name)]

        return names

def del_gid(uuid):
        db.delete("ebbinghaus_groups",
                where = "`uuid` = '%s'" % uuid)

