function refresh_content() {
        $("#content p").wrap('<div class="wcontent-p"></div>;');
        $(".li-even").wrap('<div class="wli-even"></div>;');
        $(".li-odd").wrap('<div class="wli-odd"></div>;');
}

var cur_usercb;
function jqalert(html, func) {
        alt = $("#alert");
        if(!alt.length) {
                $("body").append('<div id="alert"><p></p><br />' +
                        '<button class="pushbutton" id="alertbutton">OK</button></div>');
                $("#alertbutton").click(jqalert_cb);
                alt = $("#alert");
        }

        $("#alert p").html(html);
        cur_usercb = func;
        alt.css("display", "");
}

function jqalert_cb() {
        cur_usercb();
        alt = $("#alert").css("display", "none");
}

$(document).ready(function() {
        $("#content li:odd").addClass("li-odd"); 
        $("#content li:even").addClass("li-even"); 
        $(".pushbutton").mouseenter(function(){$(this).css("background-color", "#4A90B2");});
        $(".pushbutton").mouseleave(function(){$(this).css("background-color", "#6CF");});

        refresh_content();
});
