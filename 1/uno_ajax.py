#### uno_ajax.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

from shihira.uno_game import Game, cb
from shihira.database import load_database, unload_database, rescape
import shihira.webutil as wu

def app(environ, start_response):
        # 1. Build the game data structure reading from database.
        # 2. Process user operations if having, and update states.
        # 3. Generate AJAX.
        # 4. Destruct classes, and save data into database.

        args = wu.urlargs(environ)
        for arg in args:
                args[arg] = rescape(args[arg]).split()[0]

        load_database()
        json = ""
        game = Game(args["room"])
        args["player"] = cb(args["player"], args["room"])
        ret = game.operate(args)
        if not ret: json = game.get_ajax(args["player"])
        else: json = ret
        game.save()
        unload_database()

        status = "200 OK" 
        headers = [("Content-type", "text/plain")] 
        start_response(status, headers)
        return [json]

