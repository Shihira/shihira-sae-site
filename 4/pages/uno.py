import web

class page(object):
        def GET(self, state = ""):
                if state == "":
                        render = web.template.render("templates/", base = "framework")
                        import uuid
                        import random
                        cards = []
                        for i in range(0, 30):
                                cid = uuid.uuid4()
                                pat = random.choice("0123456789=xtfw")
                                color = random.randint(0, 3)
                                if pat in "fw": color = -1
                                cards += [(cid, color, pat)]
                        return render.uno(state, cards)

                if state == "pattern":
                        web.header("Content-Type", "image/svg+xml")
                        render = web.template.render("static/img/uno/")
                        pat = web.input().pat
                        if pat in "0123456789=x":
                                return render.uno_oval_general(web.input().pat)
                        elif pat in "tfw":
                                return getattr(render, "uno_oval_" + pat)()
                        web.header("Content-Type", "text/plain")
                        return "Pattern '%s' does not exsits." % pat
