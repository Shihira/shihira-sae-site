from dict_base import dict_base

'''
1 list  2 list  3 list  4 dict  5 def.list
[
        [], # word
        [
                [
                        {
                                "pos": "part of speech",
                                "field": "medical, maths, etc",
                                "def": [] # definition
                        }
                ], # 
                [], # relative entries
        ],
]
'''

class concise_ec(dict_base):
        def entry(self, index, no_expl = False):
                father = super(concise_ec, self)

                import re
                expl = [[], []]
                ret = father.entry(index, no_expl)

                if not no_expl:
                        def pack(lst, before = False):
                                lst = filter(bool, lst)
                                if len(lst) == 1:
                                        if before: return tuple([""] + lst)
                                        else: return tuple(lst + [""])
                                else: return tuple(lst)

                        ret_expl, expl[1] = pack(ret[1].split(u"\u76f8\u5173\u8bcd\u7ec4:\\n"))

                        ret_expl = filter(bool, ret_expl.split("\\n"))
                        expl[1] = [e.strip() for e in expl[1].split("\\n")]

                        for item in ret_expl:
                                pos, expls = pack(re.split("([a-z]+\. )", item), True)
                                field, expls = pack(re.split(u"\u3010([\s\S]+)\u3011", expls), True)
                                piece = re.split("[,|;] ", expls)
                                expl[0] += [{
                                        "pos" : pos.strip(),
                                        "field" : field,
                                        "def" : piece,
                                }]

                        ret[1] = expl

                return ret

        def __init__(self):
                father = super(concise_ec, self)

                father.__init__()
                self.load_pair("concise_ecai")
                self.load_pair("concise_ecjz")

#import random
#print concise_ec().entry(random.randint(0, 400000))

