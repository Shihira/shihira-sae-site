import web
import chemistry.equation

class page:
        def GET(self):
                ce = ""
                args = web.input()
                if hasattr(args, "ce"):
                        ce = args.ce.encode("utf-8")

                render = web.template.render("pages/templates/", base="ui_page")
                if not ce: return render.balance_ce("", "", "")
                else:
                        equation = None
                        import numpy.linalg
                        try:
                                equation = chemistry.equation.equation(ce)
                                equation.balance()
                        except numpy.linalg.linalg.LinAlgError:
                                return render.balance_ce(str(equation), equation.get_matrix(True), ce)
                        except IndexError:
                                return render.balance_ce("Failed to parse.", None, ce)
                        return render.balance_ce(str(equation), "", ce)

