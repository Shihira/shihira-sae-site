#### balance_ce.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

from numpy import linalg
import fractions
import re

def balance_ce(ce, separ="[=|+|:|_]"):
        # a matrix which rows rps atoms, and columns rps matters
        lmatrix = list()
        rmatrix = list()
        atom2line = dict()

        # get matter list form equation
        matter_list = re.split(r"\s*" + separ + "\s*", ce)
        matrix_column = len(matter_list) - 1

        for mi, matter in enumerate(matter_list):
                y = mi # get atom list from matter
                atom_list = re.findall(r"([A-Z\(\)][a-z]?)(-?[0-9]*)", matter)

                matter_list[mi] = ""
                an_times = 1
                for ai, (atom, atom_num) in enumerate(atom_list):
                        # 1 as default atom number
                        if not atom_num or atom_num == "-": atom_num += "1"

                        # process the matter expression
                        if atom == "E":
                                sign = "+"
                                if atom_num[0] == "-": sign = "-"
                                num = str(abs(int(atom_num)))
                                if num == "1": num = ""
                                matter_list[mi] += "<sup>" + num + sign + "</sup>"
                        else:
                                num = atom_num
                                matter_list[mi] += atom
                                if num == "1": num = ""
                                matter_list[mi] += "<sub>" + num + "</sub>"

                        # process parenthese
                        if atom == "(":
                                jump_over = 1
                                for i in range(ai + 1, len(atom_list)):
                                        if atom_list[i][0] == "(": jump_over += 1
                                        if atom_list[i][0] == ")":
                                                jump_over -= 1
                                                if jump_over == 0:
                                                        an_times *= int(atom_list[i][1])
                                                        break
                                continue

                        if atom == ")":
                                an_times /= int(atom_num)
                                continue

                        # add the number to matrix if the atom exists
                        if not atom in atom2line:
                                atom2line[atom] = len(atom2line)
                                lmatrix += [[0] * matrix_column]
                                rmatrix += [0]

                        x = atom2line[atom]
                        if y == matrix_column:
                                rmatrix[x] = an_times * int(atom_num)
                        else:   lmatrix[x][y] += an_times * int(atom_num)

        # solve it
        error = False
        solution = []
        try: solution = list(linalg.solve(lmatrix, rmatrix)) + [-1]
        except:
                error = True
                # to statistic the equality sign 
                separ_list = re.findall(separ, ce)
                cur_stoich = 1
                for sign in separ_list:
                        solution += [cur_stoich]
                        if sign == ":" or sign == "=":
                                cur_stoich = -1
                solution += [-1]

        for i in range(len(solution)):
                solution[i] = fractions.Fraction(solution[i]).limit_denominator()

        # find lcm of the denominator
        lcm = 1
        for i in solution:
                den = i.denominator
                lcm = lcm * den / fractions.gcd(lcm, den)
        for i in range(len(solution)):
                solution[i] *= lcm
                solution[i] = float(solution[i])

        balanced_ce = str()
        for i in range(0, len(solution)):
                sign = str()
                stoich = solution[i]
                if i > 0 :
                        if stoich * solution[i - 1] < 0:
                                sign = " = "
                        else:   sign = " + "
                stoich = str(abs(int(stoich)))
                if stoich == "1": stoich = ""
                balanced_ce += sign + stoich + str(matter_list[i])
        return error, balanced_ce

#print balance_ce("Cu+HE1+SO4E-2=CuE2+SO2+H2O")
