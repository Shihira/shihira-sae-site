// shihira.js: Shihira Fung <fengzhiping@hotmail.com>

function range(e, b, s) {
        if(b == undefined) b = 0;
        if(s == undefined) s = 1;

        var temp = [];
        for(var i = b; i < e; i += s)
                temp.push(i);
        return temp;
};

Array.prototype.choice = function(anum) {
        var num = 1;
        if(anum) num = anum;

        var indexes = range(this.length, 0);
        var temp = Array(num);
        indexes.shuffle();
        for(var i = 0; i < num; i++)
                temp[i] = this[indexes[i]];
        if(!anum) {
                return temp[0];
        }
        return temp;
};

/*
Array.prototype.shuffle = function(completely) {
        var copy = this.slice(0)
        this.sort(function() { return Math.random() - 0.5; });
        if(completely) // {
        for(var cur_i = 0; cur_i < this.length; cur_i++) {
                if(this[cur_i] == copy[cur_i])
                // search along to find an element that's fit swap
                for(var dif_i = 0; dif_i < this.length; dif_i++)
                        if(this[dif_i] != this[cur_i] &&
                           this[dif_i] != copy[cur_i]) {
                                   var t = this[cur_i];
                                   this[cur_i] = this[dif_i];
                                   this[dif_i] = t;
                        }
        }
        return this;
}
*/

Array.prototype.shuffle = function(completely) {
        var cur_idx = range(this.length, 0);
        var new_arr = []
        for(var i = 0; i < this.length; i++) {
                var rand = Math.floor(Math.random() * cur_idx.length);
                if(completely && cur_idx[rand] == i) {
                        // prevent to bring the tail to bay
                        if(cur_idx.length == 1) {
                                new_arr.push(new_arr[i-1]);
                                new_arr[i-1] = this[cur_idx[rand]];
                                break;
                        } else { i--; continue; }
                }

                new_arr.push(this[cur_idx[rand]]);
                cur_idx.splice(rand, 1);
        }

        for(var i = 0; i < this.length; i++)
                this[i] = new_arr[i];

        return this;
};

function linear_resize() {
        $(".linear").each(function() {
                var width = $(this).width();
                var column = $(this).attr("column");
                var minw = $(this).attr("minw");

                var new_width;
                for(var col = column; col >= 1; col--) {
                        new_width = width / col;
                        if(new_width >= minw) break;
                }

                $(this).children(".linear-element").css("width", new_width + "px");
        });
}

function matrix_style() {
        var border = "3px solid #999";
        $("#matrix").css("border-left", border);
        $("#matrix").css("border-right", border);
        $("#matrix td:first").css("width", "8px");
        $("#matrix tr:first td:first").css("border-top", border);
        $("#matrix tr:first td:last").css("border-top", border);
        $("#matrix td:last").css("width", "8px");
        $("#matrix tr:last td:first").css("border-bottom", border);
        $("#matrix tr:last td:last").css("border-bottom", border);
}

function article_style() {
        $("#article-content h1").each(function() {
                $("#bar-title").html(this);
        });
        $("table").addClass("table table-striped table-bordered table-hover");
}

function get_query()
{
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
        }
        return vars;
}

$(document).ready(function() {
        if($.ua) {
                if($.ua.platform.name == "android") {
                        $.fx.off = true;
                        $("*").css("transition", "none");
                } else {
                        $("body").css("font-family", "'Della Respira'");
                }
        }

        article_style();
        linear_resize();
        matrix_style();
        $(window).resize(linear_resize);

        if($("h1").length == 1)
                document.title = $("h1").text() + " - Shihira";
});
