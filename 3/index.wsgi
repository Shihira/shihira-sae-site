import web
from sae import create_wsgi_app

urls = (
"/", "pages.index.page",
"/index/?", "pages.index.page",
"/article/(.*)/?", "pages.article.page",
"/balance[-|_]ce/?", "pages.balance_ce.page",
"/ebbinghaus/(.*)/?", "pages.ebbinghaus.page",
)


###########################################################
# Need not modify

app = web.application(urls, globals())
application = create_wsgi_app(app.wsgifunc())

