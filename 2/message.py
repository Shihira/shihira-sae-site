#### message.py
# Copyright 2013 Shihira Fung.
# E-mail: fengzhiping@hotmail.com

from shihira.webutil import urlargs
from shihira.database import *
from codecs import lookup

import shihira.webutil as wu

def add_message(command, content):
        sql = "select count(*) as num from message where command='msg';"
        index = sql_query(sql)[0][0]

        sql = "insert into message values ('%s', %s, '%s');"
        arg = (command, str(index), content.replace("'", "''").replace("\\", "\\\\"))
        sql_query(sql % arg)

def get_message(command, bline = 0):
        sql = "select content from message where line_num>=%s and command='%s';"
        return sql_query(sql % (bline, command))

def insert_data(environ, args):
        if "CONTENT_LENGTH" in environ and environ["CONTENT_LENGTH"]:
                length = int(environ["CONTENT_LENGTH"])
                body = environ["wsgi.input"].read(length)
                add_message(args["command"], body)

def gen_ajax(args):
        ajax = ""

        # get message
        if "command" in args:
                bline = 0;
                if "from" in args: bline = int(args["from"])
                lines = get_message(args["command"], bline)
                wu.html_content_index = bline
                for line in lines:
                        gen_html = '<div class="left-side">%s</div>' % wu.md2html(line[0])
                        ajax = wu.html_content(gen_html) + ajax

        return ajax


def app(environ, start_response):
        status = "200 OK" 
        headers = [("Content-type", "text/html")] 
        start_response(status, headers)

        load_database()
        args = urlargs(environ)
        args["command"] = "msg"

        if "ui" not in args or args["ui"] != "0":
                html = wu.html_header
                html += wu.html_sub_title % "Message"

                html += wu.html_jquery(environ)
                html += '<script src="static/message.js"></script>\n'
                html += '<div class="input-bar">\n' \
                        '  <form>\n' \
                        '    <textarea class="submit-text" name="content" id="msg_text" style="height: 45px;"></textarea>\n' \
                        '    <button id="msg_btn" class="submit-btn">Send</button>\n' \
                        '  </form>\n' \
                        '</div>\n'

                html += wu.html_sub_title % 'History <img src="static/loading.gif" id="loading_pic"/>'
                html += '<div id="history">\n';
                html += gen_ajax(args)
                html += "</div>"
                html += wu.html_footer % ""
        else:
                insert_data(environ, args)
                html = gen_ajax(args)

        unload_database()
        return [html]

