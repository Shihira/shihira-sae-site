import web

class page(object):
        def GET(self):
                render = web.template.render("pages/templates/", base = "ui_page")
                projects = []
                projects += [("Shihira Scoreboard",
                        "/article/shihira-scoreboard",
                        "A widget getting NBA realtime score.")]
                projects += [("Chemical Equation Balancing",
                        "/balance_ce",
                        "Balance an unbalanced chemical equation; the start page has examples.")]
                projects += [("Ebbinghaus Memory Booster",
                        "/ebbinghaus/list",
                        "Help you keep things in long-term memory in line with Ebbinghaus memory curve.")]

                return render.index(projects)
