#### match_list.py
# Copyright 2013 Shihira Fung.
# E-mail: fengzhiping@hotmail.com

import sys
import urllib
import urlparse
import re
import urlparse

from shihira.translator import *
from shihira.webutil import urlargs

def generate_regex():
        teams_group = generate_group(teams_translator)
        state_group = generate_group(state_translator)

        regex = r'<a href="http://[\s\S]*?match_id=(\d+)[\s\S]*?'
        regex += teams_group + r"\s*(\d+)([-|:])(\d+)\s*"
        regex += teams_group + r"\s*" + state_group + "</a>"
        return regex

def app(environ, start_response):
        status = "200 OK" 
        headers = [("Content-type", "text/xml")] 
        start_response(status, headers)

        arg = urlargs(environ)
        url = "http://dp.sina.cn/dpool/sports/nba/index.php?" + \
                 "action=liveindex.lists&date=" + arg["date"] + "&vt=1"
        sina_data = urllib.urlopen(url).read()

        xml = '<?xml version="1.0" encoding="UTF-8"?>\n<match_list>\n'
        match_list = re.findall(generate_regex(), sina_data)
        for match in match_list:
                # generate xml
                xml += "    <match id=\"" + match[0] +  "\">\n"
                xml += "        <home>" + teams_translator[match[1]] + "</home>\n"
                xml += "        <away>" + teams_translator[match[5]] + "</away>\n"
                if match[3] == "-":
                        xml += "        <home_score>" + match[2] + "</home_score>\n"
                        xml += "        <away_score>" + match[4] + "</away_score>\n"
                else:   xml += "        <time>" + match[2] + ":" + match[4] + "</time>\n"
                xml += "        <state>" + state_translator[match[6]] + "</state>\n"
                xml += "    </match>\n"
        xml += "</match_list>\n"

        return [xml]
