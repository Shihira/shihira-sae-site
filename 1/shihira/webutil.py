#### webutil.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import urlparse
import markdown
from codecs import lookup

class urlargs(dict):
        def __init__(self, environ):
                query = urlparse.parse_qsl(environ["QUERY_STRING"])
                for item in query:
                        self[item[0]] = item[1]

html_header = '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="static/style.css" rel="stylesheet" type="text/css" />
<!-- link -->
<title>Shihira-Server</title>
</head>
<body>
<div class="page-title"><a href="start.py">Shihira Server</a></div>\n
'''

html_footer = '''
<div class="footer">
<p><em>%s</em><p>
<p>Copyright 2013 <a href="http://www.weibo.com/shihira">Shihira Fung</a>.<br />
All rights reserved.</p>
<p>Powered by Sina App Engine.</p>
</div>
</body>
</html>
'''

html_sub_title = '<div class="sub-title">%s</div>\n'
html_link = '<a href="%s">%s</a>'
html_redirect = '<meta http-equiv="refresh" content="0;%s" />'

html_content_index = 0
def html_content(text, p_wrap = True):
        global html_content_index
        if p_wrap: html = '<div class="content_%d">%s</div>'
        else: html = '<div class="content_%d"><p>%s</p></div>\n'
        html = html % (html_content_index % 2, text)
        html_content_index += 1
        return html

def md2html(text):
        content = markdown.markdown(unicode(text, "utf-8"))
        return lookup("utf-8").encode(content)[0]

def html_jquery(environ):
        return '<script src="//lib.sinaapp.com/js/jquery/1.6.2/jquery.min.js"></script>'
        # return '<script src="static/jquery.js"></script>'

def reconstruct_url(environ, path_info = True,
                script_name = False,
                query_string = False):
        from urllib import quote
        url = environ['wsgi.url_scheme']+'://'

        if environ.get('HTTP_HOST'):
                url += environ['HTTP_HOST']
        else:
                url += environ['SERVER_NAME']

                if environ['wsgi.url_scheme'] == 'https':
                        if environ['SERVER_PORT'] != '443':
                                url += ':' + environ['SERVER_PORT']
                else:
                        if environ['SERVER_PORT'] != '80':
                                url += ':' + environ['SERVER_PORT']

        if script_name: url += quote(environ.get('SCRIPT_NAME', ''))
        if path_info: url += quote(environ.get('PATH_INFO', ''))
        if environ.get('QUERY_STRING'):
                if query_string: url += '?' + environ['QUERY_STRING']
        return url


