#### UNO_ui.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import shihira.webutil as wu
from shihira.database import *

def app(environ, start_response):
        status = "200 OK" 
        headers = [("Content-type", "text/html")] 
        start_response(status, headers)

        args = wu.urlargs(environ)

        html = wu.html_header
        if "player" in args:
                player = args["player"]
                load_database()
                player_list = get_var("Player")
                ongoing = int(get_var("Ongoing"))
                if not player in player_list.split() and not ongoing:
                        set_var("Player", player_list + " " + player)
                        set_var("Message", "<b>%s</b> comes in." % player)
                unload_database()

                befr = ""
                befr += '<script>var org_url = "uno_ajax.py?player=%s";</script>\n' % player
                befr += wu.html_jquery(environ)
                befr += '<link href="static/UNO/UNO.css" rel="stylesheet" type="text/css" />\n' \
                        '<script src="static/UNO/UNO.js"></script>\n'
                html = html.replace("<!-- link -->", befr)

                html += wu.html_sub_title % 'UNO Table<br/>&nbsp;<img src="static/loading.gif" id="loading_pic"/>\n'
                wu.html_content_index = 0
                html += wu.html_content('<table width="100%" border="0"><tr>\n' \
                        '  <td id="table"></td>\n' \
                        '  <td width="100"><table id="player" width="100%" border="0"></table></td>\n' \
                        '</tr></table>\n')
                html += wu.html_content('').replace("<div", '<div id="message"')
                html += wu.html_sub_title % "Hand"
                wu.html_content_index = 0
                html += '<div id="quit" class="quit_btn">Quit</div>\n' \
                        '<div id="draw" class="oper_btn"></div>\n' \
                        '<div id="commit" class="oper_btn">Commit</div>\n' \
                        '<div id="hand"></div>\n'
        else:
                html += wu.html_sub_title % "Name, Please?"
                html += '<div class="input-bar"><form>\n' \
                        '     <input class="submit-text" type="text" name="player" value=""/>\n' \
                        '     <input class="submit-btn" type="submit" value="Play"/>\n' \
                        '</form></div>\n'
                wu.html_content_index = 0
                html += wu.html_content("<em>(Case-sensitive)<br/><br/>Chinese character, A-Z, a-z, 0-9</em>")
        html += wu.html_footer % "The copyright on UNO<sup style=\"font-size:9px\">TM</sup> is owned by Mattel."

        return [html]

