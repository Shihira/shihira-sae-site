#### uno_ajax.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import random
import shihira.webutil as wu
from shihira.database import *

class Card(object):
        color_dict = { 0: "#F00", 1: "#070",
        2: "#06C", 3: "#FC0", 4: "#666" }
        action_card = ["x", "=", "+2"]
        wildcard = ["+4", "?"]

        def __init__(self, (color, number)=(None, None)):
                if number == None:
                        if color == 4:
                                number = random.choice(wildcard)
                        else:number = random.choice(range(0, 10) +
                                self.action_card + self.wildcard)
                if color == None:
                        color = random.randint(0, 3)
                        if number in self.wildcard: color = 4

                self.color = int(color)
                self.number = str(number)

        def __str__(self):
                return '{"color":"%s","number":"%s"}' % \
                        (self.color_dict[self.color], self.number)

        def able_follow(self, card):
                if not "+" in card.number:
                        if card.color == self.color: return True
                        if card.number == self.number: return True
                        if self.color == 4: return True
                elif "+" in self.number: return True
                return False

        def able_combo(self, card):
                if card.number != self.number: return False
                else: return True

        def __cmp__(self, cardr):
                if type(cardr) != type(self): return -1
                if self.color > cardr.color:
                        return 1
                elif self.color == cardr.color:
                        if self.number > cardr.number: return 1
                        elif self.number == cardr.number: return 0
                        else: return -1
                else:   return -1

class Player(object):
        def __init__(self, name, hand = None):
                self.name = None
                self.hand = [] # hand means a card list

                self.name = name
                if hand != None:
                        self.hand = hand
                        return
                sql = "select `color`, `number` from `uno_player` " \
                      "where `player`='%s' order by `color`, `number`;" % name
                for card in sql_query(sql):
                        self.hand += [Card(card)]

        def save(self):
                # sql =  "delete from `uno_player` " \
                #        "where `player`='%s';" % self.name
                # sql_query(sql)

                if len(self.hand) == 0: return
                for card in self.hand:
                        sql = "insert into `uno_player` values ('%s', %d, '%s');" \
                                % (self.name, card.color, card.number)
                        sql_query(sql)

        def __str__(self):
                json = ""
                for card in self.hand:
                        json += "," + str(card)
                return json[1:]

class Game(object):
        def update_state(self):
                if not self.ongoing:
                        self.ongoing = 1
                        for player in self.player_list:
                                self.ongoing *= len(player.hand)
                        if self.ongoing: self.message = "Game now starts."
                else:
                        for iter_p in self.player_list:
                                if not len(iter_p.hand):
                                        self.message = r'__%s WON.__' % iter_p.name
                                        self.end_game()
                                        break

                if not len(self.player_list):
                        self.end_game()
                        self.message = ""
                if len(self.player_list):
                        self.player_active = self.player_active % len(self.player_list)

        def end_game(self):
                self.ongoing = 0
                self.player_active = 0
                self.direction = 1
                self.card_effect = 0
                self.table = Card()
                if self.table.color == 4:
                        self.table.color = random.randint(0,3)
                for player in self.player_list:
                        player.hand = []

        def __init__(self):
                # get player list
                self.player_list = []
                pn_list = get_var("Player").split()
                for pn in pn_list:
                        self.player_list += [Player(pn)]

                self.ongoing = int(get_var("Ongoing"))
                self.table = Card(tuple(get_var("Table").split()))
                self.player_active = int(get_var("PlayerActive"))
                self.card_effect = int(get_var("CardEffect"))
                self.direction = int(get_var("Direction"))
                self.message = get_var("Message")

        def save(self):
                player_str = ""
                sql_query("delete from `uno_player`;")
                for player in self.player_list:
                        player_str += " " + player.name
                        player.save()
                set_var("Ongoing", str(self.ongoing))
                set_var("Table", str(self.table.color) + " " + self.table.number)
                set_var("CardEffect", str(self.card_effect))
                set_var("Direction", str(self.direction))
                set_var("Player", player_str)
                set_var("PlayerActive", str(self.player_active));
                set_var("Message", str(self.message));

        def quit(self, player):
                player.hand = []
                self.player_list.remove(player)
                self.message = "__%s__ quits the game." % player.name

        def draw(self, player, count):
                if count > 1 and self.ongoing: self.message = \
                        "__%s__ is punished by __%s__ cards." % (player.name, count)
                elif count == 1: self.message = "__%s__ draws 1 card and skips." % player.name
                for i in range(0, count):
                        player.hand += [Card()]
                player.hand.sort()
                self.player_active += 1
                self.card_effect = 0
                if "+" in self.table.number:
                        self.table.number = "?"

        def commit(self, player, cards, color):
                # cards != card. card is the major card in cards.
                if color == -1: color = cards[0].color

                combo = True
                card_ava = None
                color_ava = False
                for card_i in cards: # detect the value above
                        combo &= cards[0].able_combo(card_i)
                        color_ava |= (card_i.color == color or card_i.color == 4)
                        if card_i.able_follow(self.table): card_ava = card_i

                if not combo: return "Cannot combo these cards."
                if card_ava == None: return "Cannot follow the table card."
                if color_ava == False: return "No such color in your card combo."
                self.table = Card((color, card_ava.number))

                for card in cards:
                        if "+" in card.number: self.card_effect += int(card.number)
                        if card.number == "=":
                                self.direction = - self.direction
                                self.message = "Direction reversed."
                        if card.number == "x":
                                self.player_active += 1
                                self.message = "__%s__ has skipped." % \
                                        (self.player_list[self.player_active % len(self.player_list)].name)
                        player.hand.remove(card)

                if len(player.hand) == 1: self.message = "%s: __UNO!__" % player.name
                self.player_active += 1

                return ""


        def operate(self, args):
                if not "oper" in args: return ""
                if not "player" in args: return "Argument error: player."

                operation = args["oper"]
                player = None
                pn = args["player"]
                for iter_p in self.player_list:
                        if pn == iter_p.name: player = iter_p
                if player == None: return "" # not on the list

                if operation == "Commit" and "card" in args and "remark" in args:
                        cards = []
                        for card_i in args["card"].split(","):
                                cards += [player.hand[int(card_i)]]
                        ret = self.commit(player, cards, int(args["remark"]))
                        self.update_state()
                        return ret
                elif operation == "Draw" and "remark" in args:
                        self.draw(player, int(args["remark"]))
                        self.update_state()
                        return ""
                elif operation == "Quit":
                        self.quit(player)
                        self.update_state()
                        return "Quit successfully."
                else: return "Argument error: remark."


        def get_ajax(self, player_name):
                player = ""
                for iter_p in self.player_list:
                        if player_name == iter_p.name:
                                player = iter_p

                json = "{"

                # all players are the same in content following
                str_player_list = ""
                for p in self.player_list: # bad name
                        str_player_list += ',{"name":"%s","hand":%d}' % \
                                (p.name, len(p.hand))
                str_player_list = "[%s]" % str_player_list[1:]
                json += '"Player":%s,' % str_player_list
                json += '"Table":%s,' % str(self.table)
                json += '"PlayerActive":%s,' % str(self.player_active)
                json += '"Message":\"%s\",' % wu.md2html(str(self.message))

                # this player's particular content
                json += '"Hand":[%s],' % str(player)
                draw_count = 0
                able_commit = False
                try:
                        if not self.ongoing and not player.hand:
                                draw_count = 7
                        elif self.player_list.index(player) == self.player_active and self.ongoing:
                                # determine the draw operations
                                if not self.card_effect: draw_count = 1
                                else: draw_count = str(self.card_effect)
                                # determine the commit operations
                                for card in player.hand:
                                        able_commit += card.able_follow(self.table)
                                if len(player.hand) == 1 and player.hand[0].number in ["x", "=", "?", "+2", "+4"]:
                                        able_commit = False
                except ValueError, AttributeError:
                        print "Anonymous player: " + player_name

                json += '"Operation_Draw":%d,' % int(draw_count)
                json += '"Operation_Commit":%s' % str(bool(able_commit)).lower()

                json += "}"
                return json


def app(environ, start_response):
        # 1. Build the game data structure reading from database.
        # 2. Process user operations if having, and update states.
        # 3. Generate AJAX.
        # 4. Destruct classes, and save data into database.

        args = wu.urlargs(environ)

        load_database()
        json = ""
        game = Game()
        ret = game.operate(args)
        if not ret: json = game.get_ajax(args["player"])
        else: json = ret
        game.save()
        unload_database()

        status = "200 OK" 
        headers = [("Content-type", "text/plain")] 
        start_response(status, headers)
        return [json]

