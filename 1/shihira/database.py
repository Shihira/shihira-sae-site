#### database.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

from sae import const
import MySQLdb

database = None
cursor = None

def sql_query(sql):
        global cursor
        # print sql
        if not cursor: load_database()
        cursor.execute(sql)
        return cursor.fetchall()

def load_database():
        global database
        global cursor

        database = MySQLdb.connect(
                 host   = const.MYSQL_HOST,
                 port   = int(const.MYSQL_PORT),
                 user   = const.MYSQL_USER,
                 passwd = const.MYSQL_PASS,
                 db     = const.MYSQL_DB)

        cursor = database.cursor()


def unload_database():
        global database
        global cursor

        database.commit()
        cursor.close()
        database.close()

def rescape(value):
        return value.replace("\\", "\\\\").replace("'", "''")

def set_var(variable, value):
        global cursor

        sql = "select count(*) from `global_var` where `variable` = '%s';"
        if sql_query(sql % variable)[0][0]:
                sql = "update `global_var` set `value` = '%s' where `variable` = '%s';"
                sql = sql % (rescape(value), variable)
                sql_query(sql)
        else:
                sql = "insert into `global_var` values ('%s', '%s');"
                sql = sql % (variable, rescape(value))
                sql_query(sql)

def get_var(variable, safe = ""):
        global cursor

        sql = "select `value` from `global_var` where `variable` = '%s';"
        try: return sql_query(sql % variable)[0][0]
        except IndexError: return safe

def del_var(variable):
        sql = "delete from `global_var` where `variable` = '%s';"
        sql_query(sql % variable);

