import web
import json

def entry_strize(entry):
        des_str = ""
        if not entry: return ""
        for d in entry[1][0]:
                if d["field"] and des_str: break
                des_str += d["pos"]
                des_str += ", ".join(d["def"][:3]) + "  "
        return des_str.strip()

def sel_dict(word, dicts = ["exconcise_ec", "concise_ec"]):
        result = None
        for d in dicts:
                exec "from dictionary_lib.{0} " \
                        "import {0} as dictionary".format(d)
                result = dictionary().search(word)
                if result: break
        return result

class page:
        def GET(self, state, word):
                query = web.input()
                result = None
                if "dict" in query:
                        result = sel_dict(word, [query.dict])
                else: result = sel_dict(word)

                data = { "w" : result[0], "des": entry_strize(result)}
                return json.dumps(data)
