import time
import uuid
import time
from utility.database import db

class memterm():
        def __init__(self, tm = "", ts = "", u = None,
                        s = 1.0, t = None):
                if not u: u = uuid.uuid1()
                if not t: t = time.time()
                self.uuid = u
                self.term = tm
                self.tips = ts
                self.rel_strength = s
                self.add_time = t

        def strength(self, toround = True):
                time_last = (time.time() - self.add_time) / 10800.0
                s = 2.718281828 ** ( - time_last / self.rel_strength)
                if toround: s = round(s * 10)
                return s

def load_terms(user):
        temp = db.select("ebbinghaus",
                what = "uuid, term, tips, add_time, rel_strength",
                where = "user = '%s'" % user)

        terms = []
        for term in temp:
                terms += [memterm(term.term, term.tips,
                        term.uuid, term.rel_strength, term.add_time)]

        return terms

def insert_term(user, term):
        db.insert("ebbinghaus",
                user = user,
                term = term.term,
                tips = term.tips,
                add_time = term.add_time,
                rel_strength = term.rel_strength,
                uuid = term.uuid)

def delete_term(uuid):
        db.delete("ebbinghaus",
                where = "uuid = '%s'" % uuid)

def update_term(uuid, state):
        add_time = time.time()
        rel_strength = db.select("ebbinghaus", what = "rel_strength",
                where = "uuid = '%s'" % uuid)[0].rel_strength
        if state: rel_strength += 0.5
        db.update("ebbinghaus", where = "uuid = '%s'" % uuid,
                add_time = add_time, rel_strength = rel_strength)

