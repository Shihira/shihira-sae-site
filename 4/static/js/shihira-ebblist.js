//author: Shihira Fung.
//email: fengzhiping@hotmail.com

$(document).ready(function() {
        $("#btn-confirm").click(function() {
                var url = "/ebbinghaus/add-" + state;
                url += "?name=" + $("#edit-add").val();
                if(state == "item") {
                        url += "&gid=" + get_query().uuid;
                        url += "&content=";
                }
                $("#help-item").text("");
                $.get(url, function(ret) {
                        if(parseInt(ret) == 2)
                                $("#help-item").html("&nbsp;&nbsp;Sorry, but we can find this word in our dictionaries");
                        else if(!parseInt(ret))
                                window.location.reload();
                });
        });

        $("#adddlg").on("shown.bs.modal", function() {
                $(":text:eq(0)").focus().select();
                $(":text:eq(0)").val("");
        });

        $("#btn-add").click(function() {
                $("#adddlg").modal();
        });

        $(".ebbitem-close").click(function(e) {
                var ebbitem = $(this).parents("tr");

                var url = "/ebbinghaus/delete-" + state;
                if(state == "group") url += "?gid=" + ebbitem.attr("uuid");
                if(state == "item") url += "?iid=" + ebbitem.attr("uuid");
                $.get(url, function(){ebbitem.fadeOut();});
                e.stopPropagation();
        });

        shortcut.add("Ctrl+A", function() { $("#btn-add").trigger("click"); });
        shortcut.add("return", function() { $("#btn-confirm").trigger("click"); });
});
