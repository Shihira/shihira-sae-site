import struct
import glob
import os

if __name__ == "__main__":
        for dfn in glob.glob("*.mark"):
                dfile = open(dfn, "rb")

                ifn = os.path.splitext(dfn)[0] + ".index"
                ifile = open(ifn, "wb")

                idata = []
                ddata = dfile.read()

                for i, c in enumerate(ddata):
                        if c == '\n':
                                s = struct.pack("L", i + 1)
                                idata += [s]

                ifile.write("".join(["\x00\x00\x00\x00"] + idata))

                dfile.close()
                ifile.close()

                os.rename(dfn, os.path.splitext(dfn)[0] + ".txt")
