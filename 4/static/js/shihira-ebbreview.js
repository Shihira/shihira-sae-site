//author: Shihira Fung.
//email: fengzhiping@hotmail.com

$(document).ready(function() {
soundManager.setup({'url':'/static/img/soundmanager2.swf'});
soundManager.onready(function() {

        ////////////////////////////////////////////////////
        // load quiz part
        var voice;
        function load_voice(word) {
                if(no_hear != "1")
                        voice = soundManager.createSound({
                                url:"//dict.youdao.com/dictvoice?type=1&audio=" + word,
                                autoLoad: !arguments[1],
                                autoPlay: arguments[2],
                        });
                else voice = { play: function() {} };
        }

        function say(word) {
                load_voice(word, true, true);
                $("#review-question").html('<span class="glyphicon glyphicon-volume-up"></span>');
                $("#review-question").click(function() { voice.play(); });
        }

        function ask(word) {
                $("#review-question").html(word);
                $("#review-question").unbind();
        }

        function load_choice(data) {
                $("#review-carea").slideDown();
                $("#review-iarea").slideUp();

                for(i in data.options) {
                        var option = data.options[i];
                        var button = $(".review-choice:eq("+i+")");
                        button.attr("class", "btn btn-block review-choice btn-info");
                        button.text(option);
                        if(i == 0) button.focus().select();
                }
        }

        function load_input(data) {
                $("#review-carea").slideUp();
                $("#review-iarea").slideDown();

                $("#review-input").css("background-color", "");
                $("#review-input").focus().select();
                $("#review-input").val("");
                $("#answer-display").html("");
        }

        var cur_data;
        function load_quiz() {
                url = "/ebbinghaus/review-quiz";
                url += "?gid=" + get_query().gid;
                url += "&skim=" + skim;
                url += "&no_hear=" + no_hear;

                $.getJSON(url, function(data) {
                        cur_data = data;
                        eval(data.bef);
                        eval(data.question);
                        if(data.type == "choice") {
                                load_choice(data);
                        } else if(data.type == "input") {
                                load_input(data);
                        }
                });
        }

        ////////////////////////////////////////////////////
        // confirm quiz part
        function confirm_choice(i) {
                $(".review-choice").attr("class", "btn btn-block review-choice btn-info");
                $(".review-choice:eq("+i+")").attr("class", "btn btn-block review-choice btn-danger");
                $(".review-choice:eq("+cur_data.answer+")").attr("class", "btn btn-block review-choice btn-success");

                return i == cur_data.answer;
        }

        function confirm_input() {
                var ret = $("#review-input").val() == cur_data.answer;
                if(ret) $("#review-input").css("background-color", "#DFD");
                else $("#review-input").css("background-color", "#FDD");

                return ret;
        }

        $(".review-choice").click(function() {
                var index = $(this).index(".review-choice");
                if(cur_data.type != "choice") return;
                if(confirm_choice(index)) {
                        $.get("/ebbinghaus/review-correct?skim=" + skim +
                                "&iid=" + cur_data.uuid);
                }
                else {
                        $.get("/ebbinghaus/review-wrong?skim=" + skim +
                                "&iid=" + cur_data.uuid);
                }
                eval(cur_data.aft);
                setTimeout(load_quiz, 2000);
        });

        $("#review-submit").click(function() {
                if(cur_data.type != "input") return;
                if(!cur_data.finished) {
                        if(confirm_input()) {
                                $.get("/ebbinghaus/review-correct?skim=" + skim +
                                        "&iid=" + cur_data.uuid);
                                setTimeout(load_quiz, 2000);
                        }
                        else {
                                if(cur_data.answer.split().length <= 3)
                                        $.get("/ebbinghaus/review-wrong?skim=" + skim +
                                                "&iid=" + cur_data.uuid);
                                $("#answer-display").html(cur_data.answer);
                        }
                        eval(cur_data.aft);
                        cur_data.finished = true;
                } else  load_quiz();
        });

        $("#review-input").keypress(function(event) {
                if(event.which == 13) {
                        $("#review-submit").trigger("click");
                }
        });

        $("#review-dontknow").click(function() {
                if(cur_data.type == "input") {
                        confirm_input();
                }
                if(cur_data.type == "choice") {
                        $(".review-choice:eq("+cur_data.answer+")").attr("class", "btn btn-block review-choice btn-success");
                }
                $.get("/ebbinghaus/review-wrong?iid=" + cur_data.uuid);
                setTimeout(load_quiz, 2000);
                eval(cur_data.aft);
        });

        $("#review-pass").click(function() {
                $.get("/ebbinghaus/review-familiar?iid=" + cur_data.uuid);
                load_quiz();
        });

        shortcut.add("Pagedown", function(){$("#review-dontknow").trigger("click");});
        shortcut.add("Ctrl+R", function(){$("#review-question").trigger("click");});
        shortcut.add("1", function(){$(".review-choice:eq(0)").trigger("click");});
        shortcut.add("2", function(){$(".review-choice:eq(1)").trigger("click");});
        shortcut.add("3", function(){$(".review-choice:eq(2)").trigger("click");});
        shortcut.add("4", function(){$(".review-choice:eq(3)").trigger("click");});

        load_quiz();
});
});
