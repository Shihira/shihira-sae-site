import web
import json
from utility.database import db
import ebbinghaus_lib.ebbgroup as ebbgroup
import ebbinghaus_lib.ebbitem as ebbitem

class page:
        def GET(self, state = "group"):
                render = web.template.render("templates/", base="framework")
                query = web.input()
                data = []

                uid = "132456798"

                ### group ###########################################
                if state == "add-group":
                        web.header('Content-type','text/plain')
                        new_group = ebbgroup.from_text(uid, query.name)
                        new_group.dump()
                        return "Success"

                elif state == "delete-group":
                        web.header('Content-type','text/plain')
                        ebbgroup.del_gid(query.gid)
                        return "Success"

                elif state == "group":
                        page = 0
                        if "p" in query: page = int(query.p)
                        groups = ebbgroup.from_uid(uid)
                        for group in groups[page * 30:30]:
                                data += [[group.uuid, group.name,
                                        group.get_items_digest(), group.get_items_count()]]
                        gb = {"page":page, "pcount": (len(groups) + 29) / 30, "uuid": ""}
                        return render.ebbinghaus(state, data, gb)


                ### item ############################################
                elif state == "add-item":
                        web.header('Content-type','text/plain')
                        # If there's no such word in the dict, make it an error
                        from dictionary import sel_dict
                        if not sel_dict(query.name): return "2:No Such Word in Dictionaries"
                        new_item = ebbitem.from_text(query.gid, query.name, query.content)
                        new_item.dump()
                        return "Success"

                elif state == "delete-item":
                        web.header('Content-type','text/plain')
                        ebbitem.del_iid(query.iid)
                        return "Success"

                elif state == "item":
                        from dictionary import entry_strize, sel_dict
                        page = 0
                        if "p" in query: page = int(query.p)
                        cur_group = ebbgroup.from_gid(query.uuid)
                        items = cur_group.get_items()
                        for i in items[page * 30:page * 30 + 29]:
                                se = int(round(i.strength_coef() * 100))
                                if se < 0: se = 0
                                data += [[i.uuid, i.item,
                                        entry_strize(sel_dict(i.item)),
                                        str(se) + "%"]]
                        gb = {"page":page, "pcount": (len(items) + 29) / 30, "uuid": query.uuid}
                        return render.ebbinghaus(state, data, gb)

                ### review ###########################################
                elif state == "review":
                        return render.ebbinghaus(state, data, { "skim" : query.skim, "no_hear" : query.no_hear })

                elif state == "review-correct":
                        import time
                        web.header('Content-type','text/plain')
                        cur_item = ebbitem.from_iid(query.iid)
                        if query.skim == "0":
                                cur_item.strength += 1
                        cur_item.time = time.time()
                        cur_item.dump()
                        return "Success"
                        
                elif state == "review-familiar":
                        import time
                        web.header('Content-type','text/plain')
                        cur_item = ebbitem.from_iid(query.iid)
                        cur_item.strength = (cur_item.strength / 4 + 1) * 4
                        cur_item.time = time.time()
                        cur_item.dump()
                        return "Success"

                elif state == "review-wrong":
                        import time
                        web.header('Content-type','text/plain')
                        cur_item = ebbitem.from_iid(query.iid)
                        cur_item.strength = (cur_item.strength / 4 - 1) * 4 + 1
                        if cur_item.strength < 5:
                                cur_item.strength = 5
                        print cur_item.strength
                        cur_item.time = time.time()
                        cur_item.dump()
                        return "Success"

                elif state == "review-quiz":
                        web.header('Content-type','text/json')
                        import ebbinghaus_lib.ebbquiz as ebbquiz
                        import random
                        sel_item = ebbquiz.rand_item(query.gid, query.skim)

                        func = [
                                ebbquiz.quiz_kitem_ccontent,
                                ebbquiz.quiz_kcontent_citem,
                                ebbquiz.quiz_kcontent_iitem,
                                ebbquiz.quiz_hitem_ccontent,
                                ebbquiz.quiz_hitem_iitem,
#                                ebbquiz.quiz_ktrans_iexample
                        ]

                        func_3 = [
                                ebbquiz.quiz_kcontent_iitem,
                                ebbquiz.quiz_hitem_iitem,
#                                ebbquiz.quiz_ktrans_iexample
                        ]

                        func_nohear = [
                                ebbquiz.quiz_kitem_ccontent,
                                ebbquiz.quiz_kcontent_citem,
                                ebbquiz.quiz_kcontent_iitem,
                        ]

                        if "no_hear" in query and query.no_hear == "1":
                                sel_func = random.choice(func_nohear)
                        elif sel_item.strength_coef() % 4 == 3:
                                sel_func = random.choice(func_3)
                        else:   sel_func = random.choice(func)
                        import time
                        print sel_item.strength, time.time() - sel_item.time, sel_item.strength_coef()

                        return json.dumps(sel_func(sel_item))
                else:
                        raise web.notfound()


