import web

class page(object):
        def GET(self):
                render = web.template.render("templates/", base = "framework")
                projects = [
                        ("CE Balancer",
                        "ceb.jpg", "/ce_balancer",
                        "CE Balancer calculates the stoichiometry of chemical euqations "
                        "whose matters are completed and correct."),

                        ("UNO&trade; Online",
                        "uno.jpg", "/uno",
                        "UNO&trade; is the most popular card game in the world! Now you are able "
                        "to play it with friends through the Internet."),

                        ("Memory Booster",
                        "ebb.jpg", "/ebbinghaus",
                        "Find it too hard to memorize new vocabularies? Ebbinghaus helps "
                        "you expand your memory with a regular learning schedule!"),
                ]

                return render.index(projects)
