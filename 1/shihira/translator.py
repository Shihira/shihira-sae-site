﻿#-*- coding:utf-8 -*-

#### translator.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

def generate_group(translator):
        group = "("
        for item in translator:
                group += item
                group += "|"
        group = group[:len(group) - 1] + ")"
        return group

teams_translator = {
"尼克斯": "NYK",
"篮网": "NJ",
"凯尔特人": "BOS",
"猛龙": "TOR",
"76人": "PHI",
"活塞": "DET",
"步行者": "IND",
"骑士": "CLE",
"雄鹿": "MIL",
"公牛": "CHI",
"热火": "MIA",
"魔术": "ORL",
"奇才": "WAS",
"老鹰": "ATL",
"山猫": "CHA",
"森林狼": "MIN",
"掘金": "DEN",
"爵士": "UTH",
"开拓者": "POR",
"雷霆": "OKC",
"国王": "SAC",
"湖人": "LAL",
"太阳": "PHO",
"勇士": "GS",
"快船": "LAC",
"马刺": "SAS",
"火箭": "HOU",
"小牛": "DAL",
"灰熊": "MEM",
"黄蜂": "NO "
}

state_translator = {
"第1节": "1",
"第2节": "2",
"第3节": "3",
"第4节": "4",
"完场": "5",
}


