#### message.py
# Copyright 2013 Shihira Fung.
# E-mail: fengzhiping@hotmail.com

from shihira.webutil import urlargs
import shihira.users as users
import urllib as ul
from shihira.database import *
from codecs import lookup

import shihira.webutil as wu

def get_message(command, bline = 0):
        sql = "select content from message where line_num>=%s and command='%s';"
        return sql_query(sql % (bline, command))

def insert_data(environ, args, user):
        if "CONTENT_LENGTH" in environ and environ["CONTENT_LENGTH"]:
                length = int(environ["CONTENT_LENGTH"])
                content = ul.quote(wu.md2html(environ["wsgi.input"].read(length)))

                user.get_users_show()
                body = 'piu="%s" addr="%s" sn="%s" cont="%s"' % (user.profile_image_url,
                        "http://www.weibo.com/u/" + str(user.uid),
                        user.screen_name, content)

                sql = "select count(*) as num from message where command='%s';" % args["command"]
                index = sql_query(sql)[0][0]

                sql = "insert into message values ('%s', %s, '%s');"
                arg = (args["command"], index, body)
                sql_query(sql % arg)

def gen_ajax(args):
        ajax = ""

        # get message
        if "command" in args:
                bline = 0;
                if "from" in args: bline = int(args["from"])
                lines = get_message(args["command"], bline)
                wu.html_content_index = bline
                for line in lines:
                        gen_html = '<div class="left-side" %s></div>' % line[0]
                        ajax = wu.html_content(gen_html) + ajax

        return ajax


def app(environ, start_response):
        load_database()
        args = urlargs(environ)
        if "command" not in args:
                args["command"] = "default"

        # must login before using
        if "user" not in args:
                html = wu.html_redirect % users.get_redirect_uri(environ)
        elif "ui" not in args or args["ui"] != "0":
                html = wu.html_header.replace("<!-- link -->", "%s")
                html = html % '<link href="static/message.css" rel="stylesheet" type="text/css" />'
                html += wu.html_sub_title % "Message"

                html += wu.html_jquery(environ)
                html += '<script>ajax_req = "message.py?ui=0&command=%s&user=%s";</script>' % (args["command"], args["user"])
                html += '<script src="static/message.js"></script>\n'
                html += '<div class="input-bar">\n' \
                        '  <form>\n' \
                        '    <textarea class="submit-text" name="content" id="msg_text" style="height: 45px;"></textarea>\n' \
                        '    <a id="msg_btn" class="submit-btn">Send</a>\n' \
                        '  </form>\n' \
                        '</div>\n'

                html += wu.html_sub_title % 'History <img src="static/loading.gif" id="loading_pic"/>'
                html += '<div id="history">\n';
                html += gen_ajax(args)
                html += "</div>"
                html += wu.html_footer % ""
        else:
                user = users.User(environ)
                insert_data(environ, args, user)
                html = gen_ajax(args)

        unload_database()

        status = "200 OK" 
        headers = [("Content-type", "text/html")] 
        start_response(status, headers)

        return [html]

