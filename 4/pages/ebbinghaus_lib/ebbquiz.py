from ..dictionary import entry_strize, sel_dict

prep = ["about","at","as","by","away","for","form","in","into","of","off",
        "on","over","to","with","down","up","out","out of","upon"]
dire = ["across","agianst","around","aside","forward","through","back",
        "after","below","beside","but"]
verb = ["have","make","do","take","get","put","set"]
some = ["sb.", "sth.", "sw.", "sh."]

wrds = []
def similar(word):
        global wrds
        import random
        import utility.ngram as ngram
        from ..dictionary_lib.exconcise_ec import exconcise_ec
        if not wrds:
                wrds = exconcise_ec().fetch_all()
                wrds = ngram.NGram(wrds)

        alter = wrds.search(word)[:20] #TODO
        alter = zip(*alter)[0]
        alter = random.sample(alter, 4)
        if not word in alter:
                alter[random.randint(0, 3)] = word
        random.shuffle(alter)
        index = alter.index(word)

        return index, [w.decode("ascii") for w in alter]

def rand_item(gid, skim):
        import random
        import ebbgroup
        items = ebbgroup.from_gid(gid).get_items()
        strgth = 0
        skim = int(skim)

        if not skim:
                for i in range(0, len(items)):
                        strgth += 1 - items[i].strength_coef()
                        items[i] = (strgth, items[i])
                rnum = random.uniform(0, strgth)
                for item in items:
                        if item[0] >= rnum :
                                return item[1]
        elif skim: return random.choice(items)

def rand_def(word):
        import random
        entry = sel_dict(word)
        def_items = random.choice(entry[1][0])
        return def_items["pos"] + random.choice(def_items["def"]).strip()

def quiz_kcontent_citem(item):
        quiz = {}
        answer, words = similar(item.item)
        options = []

        quiz["type"] = "choice"
        quiz["uuid"] = item.uuid
        quiz["question"] =  'ask("%s");' % rand_def(item.item)
        quiz["options"] = words
        quiz["answer"] = answer

        quiz["bef"] = 'load_voice("%s");' % item.item
        quiz["aft"] = 'voice.play();';

        return quiz

def quiz_kitem_ccontent(item):
        quiz = {}
        answer, words = similar(item.item)
        options = []
        for w in words:
                options += [rand_def(w)]

        quiz["type"] = "choice"
        quiz["uuid"] = item.uuid
        quiz["question"] = 'ask("%s");' % item.item
        quiz["options"] = options
        quiz["answer"] = answer
        quiz["voice"] = item.item

        quiz["bef"] = 'load_voice("%s");' % item.item
        quiz["aft"] = 'voice.play();'

        return quiz

def quiz_hitem_ccontent(item):
        quiz = {}
        answer, words = similar(item.item)
        options = []
        for w in words:
                options += [rand_def(w)]

        quiz["type"] = "choice"
        quiz["uuid"] = item.uuid
        quiz["question"] = 'say("%s");' % item.item
        quiz["options"] = options
        quiz["answer"] = answer
        quiz["voice"] = item.item

        quiz["bef"] = ''
        quiz["aft"] = r"""ask('<span class="text-info">%s</span>');""" % item.item

        return quiz
def quiz_kcontent_iitem(item):
        quiz = {}

        quiz["type"] = "input"
        quiz["uuid"] = item.uuid
        quiz["question"] = 'ask("%s");' % entry_strize(sel_dict(item.item))
        quiz["answer"] = item.item
        quiz["voice"] = item.item

        quiz["bef"] = 'load_voice("%s");' % item.item
        quiz["aft"] = 'voice.play();'

        return quiz

def quiz_ktrans_iexample(item):
        import random, re
        result = sel_dict(item.item, ["egstnc"])[1]
        result = random.choice(result)
        result[0] = re.sub(u", ie .+", "", result[0])
        result[0] = re.sub(u"\(ie .+\)", "", result[0])
        result[0] = re.sub(u", eg .+", "", result[0])
        result[1] = re.sub(u"\uff08.+\uff09", "", result[1])

        if result[1][-1:] == ".": result[1] = result[1][:-1]
        if result[0][-1:] == ".": result[0] = result[0][:-1]

        quiz = {}

        quiz["type"] = "input"
        quiz["uuid"] = item.uuid
        quiz["question"] = r'ask("%s (%s)");' % (result[1], item.item)
        quiz["answer"] = result[0]
        quiz["voice"] = item.item

        quiz["bef"] = ''
        quiz["aft"] = ''

        return quiz

def quiz_hitem_iitem(item):
        quiz = {}

        quiz["type"] = "input"
        quiz["uuid"] = item.uuid
        quiz["question"] = 'say("%s");' % item.item
        quiz["answer"] = item.item
        quiz["voice"] = item.item

        quiz["bef"] = ""
        quiz["aft"] = r"""ask('<span class="text-info">%s</span>');""" % entry_strize(sel_dict(item.item))

        return quiz
