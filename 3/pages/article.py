import web
import utility.webpage as wp

class page:
        def GET(self, article):
                render = web.template.render("pages/templates/", base="ui_page")
                file_md = open("article/%s.md" % article, "r").read()
                if file_md[0] != "<": return render.article(wp.md2html(file_md))
                else: return render.article(file_md)

