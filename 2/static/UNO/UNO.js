var sel_hand_card = new Array();
var url = org_url;

$(document).ready(function(){

function cssrewrite(value, func, css) { if(value != func(css)) func(css, value); }
function htmlrewrite(value, func) { if(value != func) func(value); }

function update() {
        $("#loading_pic").css("display", "");
        $.get(url, function(data) { 
                if(data[0] != "{") {
                        $("#loading_pic").css("display", "none");
                        alert(data);
                        return;
                }
                var game_info = $.parseJSON(data);

                ///////////////////////////////////////////////////// Player
                var player_html = "";
                for(var i = 0; i < game_info.Player.length; i++) {
                        var tr_class;
                        if(i == game_info.PlayerActive)
                                tr_class = "player-active";
                        else    tr_class = "content_" + (i + 1) % 2;
                        player_html += '<tr class="' + tr_class + '"><td>' +
                                game_info.Player[i].name + '</td><td width="30">' +
                                game_info.Player[i].hand + '</td></tr>\n';
                }
                $("#player").html(player_html);

                ///////////////////////////////////////////////////// Notification
                $("#message").html(game_info.Message);

                ///////////////////////////////////////////////////// Table
                $("#table").attr("uno_color", game_info.Table.color);
                $("#table").attr("uno_number", game_info.Table.number);
                if(game_info.Table.number == "x") game_info.Table.number = '<img style="margin:25px" src="static/UNO/Skip.png" />';
                if(game_info.Table.number == "?") game_info.Table.number = '';
                if(game_info.Table.number == "=") game_info.Table.number = '<img style="margin:25px" src="static/UNO/Reverse.png" />';
                var table_html = '<div id="table-card">' + game_info.Table.number +'</div>';
                if($("#table").html() != table_html) $("#table").html(table_html);
                $("#table-card").css("background-color", game_info.Table.color);

                ///////////////////////////////////////////////////// Hand
                var hand_html = "";
                for(var i = 0; i < game_info.Hand.length; i++) {
                        card = game_info.Hand[i];
                        orgnum = card.number;
                        orgclr = card.color;
                        if(orgnum == "x") card.number = '<img src="static/UNO/Skip.png" />';
                        if(orgnum == "?") card.number = '';
                        if(orgnum == "=") card.number = '<img src="static/UNO/Reverse.png" />';
                        if(orgclr == "#666") card.color = "#666; background-image:url(static/UNO/Colorful.png)";
                        hand_html += '<span onclick="hand_card_click(' + i + ');" ' +
                                'class="hand-card" style="background-color:' + card.color +
                                ';" uno_color="' + orgclr + '" uno_number="' + orgnum + '">' +
                                card.number + '</span>';
                }
                $("#hand").html(hand_html);

                ///////////////////////////////////////////////////// Operation
                if(game_info.Operation_Draw) {
                        $("#draw").css("display", "");
                        $("#draw").html("Draw " + game_info.Operation_Draw);
                } else  $("#draw").css("display", "none");
                $("#draw").attr("remark", game_info.Operation_Draw);

                if(game_info.Operation_Commit) {
                        $("#commit").css("display", "");
                } else  $("#commit").css("display", "none");

                show_hand_card();
                $("#loading_pic").css("display", "none");

                // setTimeout(update, 5000);
        });
        url = org_url;
}

$("#draw").click(function() {
        url += "&oper=Draw&remark=" + $("#draw").attr("remark");
        update();
});

$("#commit").click(function() {
        function card_sel() {
                return $(".hand-card:eq(" + sel_hand_card[0] + ")");
        }

        color = card_sel().attr("uno_color");
        number = card_sel().attr("uno_number");

        if(sel_hand_card.length == 0) return;
        wildcolor = "-1";
        if(color == "#666" || sel_hand_card.length > 1) {
                wildcolor = prompt("Select a color:\n    0 - Red\n    1 - Green\n" +
                        "    2 - Blue\n    3 - Yellow", "0");
                if(!wildcolor) return;
                if(parseInt(wildcolor) > 3) wildcolor = "3";
                if(parseInt(wildcolor) < 0) wildcolor = "0";
        }
        url += "&oper=Commit&card=" + sel_hand_card + "&remark=" + wildcolor;
        sel_hand_card = [];
        update();
});

$("#quit").click(function() {
        url += "&oper=Quit";
        update();
        setTimeout(function(){window.location.href="UNO_ui.py";}, 500);
});

update();
setInterval(update, 3000);

});

function show_hand_card() {
        $(".hand-card").css("border", "3px solid #FFF"); 
        for(hc_index in sel_hand_card)
                $(".hand-card:eq(" + sel_hand_card[hc_index] +")").
                        css("border", "3px solid #F63");
}

function hand_card_click(i) {
        var index_in_shc = sel_hand_card.indexOf(i);
        if(index_in_shc == -1) sel_hand_card.push(i);
        else sel_hand_card.splice(index_in_shc, 1);

        show_hand_card();
}

