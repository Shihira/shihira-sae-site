#### authorize.py
# Copyright(C) 2013 Shihira Fung.
# e-mail: fengzhiping@hotmail.com

import shihira.webutil as wu
import shihira.weibo as weibo
import shihira.users as users
from shihira.database import *
import time

def app(environ, start_response):
        args = wu.urlargs(environ)
        html = ""

        client = users.get_client(wu.reconstruct_url(environ))

        if not "code" in args:
                html += wu.html_header
                html += wu.html_content(wu.html_link % ( client.get_authorize_url(state = 
                        args["url"]), '<img src="http://www.sinaimg.cn/blog/developer/wiki/48.png" />'))
                html += wu.html_footer % ""

        else:
                load_database()
                code = args["code"]
                rat = client.request_access_token(code)

                access_token = rat.access_token
                expires_in = rat.expires_in
                uid = rat.uid
                client.set_access_token(access_token, expires_in)
                create_at = int(time.time())

                # users_show
                us = client.users.show.get(uid = uid)
                screen_name = us.screen_name

                sql = "delete from `access_tokens` where `uid` = %s;\n" % uid
                sql_query(sql)
                sql = "insert into `access_tokens` values (%s,'%s','%s',%s,'%s',%s);" \
                      % (uid, code, access_token, expires_in, screen_name, create_at)
                sql_query(sql)

                html = '<meta http-equiv="refresh" content="0;%s" />' % (args["state"] + "?user=" + str(uid))
                unload_database()

        status = "200 OK" 
        headers = [("Content-type", "text/html")] 
        start_response(status, headers)

        return html
